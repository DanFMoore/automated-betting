<?php

/**
 * Class whose primary methods consist of adding many requests for urls with multiple calls with addRequest() and then retrieving with getRequest().
 * Both methods take the url as a parameter. The class fetches all queued requests at the same time using curl multi.
 */
class restClient
{
	/**
	 * Configurable data
	 */
	const BASE_URL = 'https://api.games.betfair.com/rest/v1/';
	const POST_FIELD = 'xmlRequest';

	private $urlParams = array("username" => "gnuffo1");
	private $headers = array(
		"gamexAPIPassword" => "victwat1",
		"gamexAPIAgent" => "bob@home.com.mysap.1",
		"gamexAPIAgentInstance" => "37t9yt78wyow874ynctow87tcno845"
	);

	/**
	 * End of Configurable data
	 */
	private $sessions = array();
	private $results = array();
	private $requestTime;
	private $responseTime;
	private $multi;

	public function __construct()
	{
		foreach($this->headers as $key => $header)
		{
			$this->headers[] = "$key: $header";
			unset($this->headers[$key]);
		}

		$this->urlParams = http_build_query($this->urlParams);
	}

	public function getRequestTime()
	{
		return $this->requestTime;
	}

	public function getResponseTime()
	{
		return $this->responseTime;
	}

	/**
	 * Queue up a request for the url. Retrieve with getRequest() and pass the same url.
	 * @param string $url
	 */
	public function addRequest($url)
	{
		if($session = $this->makeSession($url))
		{
			$this->sessions[$url] = $session;
		}
	}

	private function makeSession($url)
	{
		if(strpos($url, '?'))
		{
			$url .= '&' . $this->urlParams;
		}
		else
		{
			$url .= '?' . $this->urlParams;
		}

		if($session = curl_init(self::BASE_URL . $url))
		{
			curl_setopt($session, CURLOPT_ENCODING, 'gzip');
			curl_setopt($session, CURLOPT_HTTPHEADER, $this->headers);
			curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);

			return $session;
		}
	}

	public function hasUrl($url)
	{
		if(isset($this->results[$url]) OR isset($this->sessions[$url]))
		{
			return true;
		}
	}

	/**
	 * Get data for request without needing to add to queue. (proxies adding to queue then retrieving)
	 * @param string $url
	 * @return SimpleXmlElement
	 */
	public function getSingleRequest($url, $data = null)
	{
		$this->requestTime = microtime(true);
		
		$session = $this->makeSession($url);
		
		if(!$this->multi)
		{
			$this->multi = curl_multi_init();
		}

		$multi = $this->multi;
		
		if($data)
		{
			if(is_object($data))
			{
				$data = $data->asXml();
			}
		
			$data = urlencode($data);		
			curl_setopt($session, CURLOPT_POST, 1);
			curl_setopt($session, CURLOPT_POSTFIELDS, self::POST_FIELD . '=' . $data);
		}
		
		curl_multi_add_handle($multi, $session);
		$this->runMulti($multi);
		$content = '';

		try
		{
			$content = curl_multi_getcontent($session);
			$result = new SimpleXmlElement($content);
		}
		catch(Exception $e)
		{
			throw new restClientException($e, $content);
		}
	
		curl_multi_remove_handle($multi, $session);	
		
		$this->responseTime = microtime(true);
		return $result;
	}
	
	/**
	 * Post data to the rest server
	 * @param string $url
	 * @param SimpleXmlElement|string $data
	 * @param int $repeat - 0 not to repeat; to send it once (default), 1 or more to send it two or more times
	 * @return SimpleXmlElement if no repeat; array of SimpleXmlElements if repeated
	 */
	public function post($url, $data)
	{
		return $this->getSingleRequest($url, $data);
	}

	/**
	 * Retrieve the data for the given url previously added from addRequest().
	 * @param string $url
	 * @return SimpleXmlElement
	 */
	public function getRequest($url)
	{
		if(!isset($this->results[$url]))
		{
			if(isset($this->sessions[$url]))
			{
				$this->getRequests();
			}
			else
			{
				throw new Exception('Request not added: ' . $url);
			}
		}

		$result = $this->results[$url];
		unset($this->results[$url]);

		return $result;
	}

	private function getRequests()
	{
		$this->requestTime = microtime(true);

		if(!$this->multi)
		{
			$this->multi = curl_multi_init();
		}

		$multi = $this->multi;

		foreach($this->sessions as $session)
		{
			curl_multi_add_handle($multi, $session);
		}
		
		$this->runMulti($multi);

		foreach($this->sessions as $url => $session)
		{
			$content = curl_multi_getcontent($session);

			try
			{
				$this->results[$url] = new SimpleXmlElement($content);
			}
			catch(Exception $e)
			{
				unset($this->multi);
				throw new restClientException($e, $content);
			}

			curl_multi_remove_handle($multi, $session);
		}
		
		$this->sessions = array();
		$this->responseTime = microtime(true);
	}

	/**
	 * Execute a curl multi object
	 * @param resouce $multi - curl multi resource object
	 */
	private function runMulti($multi)
	{
		// Set the time limit for this function to 10 seconds.
		// It will die and restart the script if it exceeds it.
		set_time_limit(10);

		$active = null;

		// Execute the handles
		do
		{
			$mrc = curl_multi_exec($multi, $active);
		}
		while($mrc == CURLM_CALL_MULTI_PERFORM);

		while($active && $mrc == CURLM_OK)
		{
			if(curl_multi_select($multi) != -1)
			{
				usleep(100000);
			}
			do
			{
				$mrc = curl_multi_exec($multi, $active);
			}
			while($mrc == CURLM_CALL_MULTI_PERFORM);
		}
		
		// Don't forget to take off the limit if it's successful
		set_time_limit(0);
	}
}