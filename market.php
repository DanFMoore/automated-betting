<?php

class market
{
	private $id;
	private $status;
	private $layBook;
	private $backBook;
	private $selections = array();
	private $updatedSelections = array();
	private $winningSelections = array();
	private $game;
	private $commissionRate;
	private $type;
	private $snapshot;
	private $selectionType;

	public function __construct($xml, $snapshot)
	{
		$this->id = (int) $xml['id'];
		$this->game = $snapshot->getGame();
		$this->selectionType = (string) $xml->selections['type'];

		$this->snapshot = $snapshot;
		$this->commissionRate = (float) $xml->commissionRate;
		$this->status = (string) $xml->status;
		$this->type = (string) $xml->marketType;

		$this->calcSelections($xml->selections->selection);
		$this->calcBook();
		$this->calcTickDifferences();
	}

	public function update($new)
	{
		$this->updatedSelections = $new->getSelections();
	}

	/**
	 * Generate the $this->selections array for this market, for any selections we have an interest in.
	 * @see getSelections()
	 */
	private function calcSelections($selections)
	{
		$this->checkValidStatuses($selections);

		foreach($selections as $selection)
		{
			if(!$this->isSelectionInteresting($selection))
			{
				continue;
			}

			//if any selection is named "Tie", it cannot be used in any system that assumes the mathmatical book is 1
			if($selection->name == 'Tie')
			{
				continue;
			}

			$selectionArray = array();

			$selectionArray['amountMatched'] = (float) $selection->amountMatched;
			$selectionArray['status'] = (string) $selection->status;
			$selectionArray['id'] = (int) $selection['id'];

			if($selection->status == 'IN_PLAY')
			{
				foreach(range(0, 2) as $i) //go through each of the three best prices and stick the details in the backs and lays array
				{
					$array['price'] = (float) $selection->bestAvailableToBackPrices->price[$i];
					$array['unmatched'] = (float) $selection->bestAvailableToBackPrices->price[$i]["amountUnmatched"];

					$selectionArray['backs'][] = $array;

					$array["price"] = (float) $selection->bestAvailableToLayPrices->price[$i];
					$array["unmatched"] = (float) $selection->bestAvailableToLayPrices->price[$i]["amountUnmatched"];

					$selectionArray['lays'][] = $array;
				}
			}

			$this->selections[$selectionArray['id']] = $selectionArray;
		}
	}

	/**
	 * Make sure the selections in the market have valid statuses for the market's status.
	 * At the moment the only check is that there isn't a winner with other selections still in play.
	 *
	 * @throws marketStatusException
	 */
	private function checkValidStatuses($selections)
	{
		// The below check only covers single winner or winner / tie markets
		if(!$this->hasSingleWinner())
		{
			return;
		}

		foreach($selections as $selection)
		{
			// If selection is a WINNER, then make sure no other selections in market are IN_PLAY
			if('WINNER' == $selection->status)
			{
				foreach($selections as $selection)
				{
					if('IN_PLAY' == $selection->status)
					{
						throw new marketStatusException('WINNER with IN_PLAY selections', $this->snapshot->getRawXml());
					}
				}

				break;
			}
		}
	}

	/**
	 * Do we have an interest in this selection?
	 * If the game is running, we're only interested in selections that are still in play; they can have already lost
	 * (e.g. poker - a hand could have lost but the others are still in contention).
	 * If the game is over, then we only care if they are settled so we can check how much money we may have had matched.
	 * 
	 * @return bool
	 */
	private function isSelectionInteresting($selection)
	{
		switch($this->status)
		{
			case 'SUSPENDED_GAME_ROUND_OVER':
			case 'SUSPENDED_GAME_SETTLING':
				return in_array($selection->status, array('WINNER', 'LOSER', 'TIED_DEAD_HEAT'));

			case 'ACTIVE':
				return 'IN_PLAY' == $selection->status;
		}

		return false;
	}

	private function calcTickDifferences()
	{
		foreach($this->selections as $id => $selection)
		{
			foreach(array('backs', 'lays') as $type)
			{
				if(!isset($selection[$type]))
				{
					continue;
				}

				foreach($selection[$type] as $key => $price)
				{
					//no prices for this selection/price type; move onto next selection/price type
					if($price['price'] == 0)
					{
						break 2;
					}

					if($key == 0)
					{
						$bestPrice = $price['price'];
					}
					elseif($key > 0)
					{
						$difference = $this->getGame()->getTickDifference($bestPrice, $price['price']);
						$this->selections[$id][$type][$key - 1]['tickDifference'] = $difference;

						$bestPrice = $price['price'];
					}
				}
			}
		}
	}

	/**
	 * Calculates the book percentages.
	 */
	private function calcBook()
	{
		if(isset($this->backBook) OR isset($this->layBook))
		{
			return;
		}

		if(!$this->selections OR $this->status != 'ACTIVE')
		{
			$this->backBook = null;
			$this->layBook = null;
			return;
		}

		$this->backBook = 0; //Safety first
		$this->layBook = 0;

		foreach($this->selections as $selection)
		{
			//At this point, the hands must still be in play, but they might not have any prices. If this is the case,
			//set their prices to a figure so that the book will never be in our favour
			if(!isset($selection['backs']) OR $selection['backs'][0]['price'] == 0)
			{
				$selection['backs'][0]['price'] = 1;
			}

			if(!isset($selection['lays']) OR $selection['lays'][0]['price'] == 0)
			{
				$selection['lays'][0]['price'] = -1;
			}

			$this->backBook += 1 / $selection['backs'][0]['price'];
			$this->layBook += 1 / $selection['lays'][0]['price'];
		}
	}

	/**
	 * Get the market prices for each selection we have an interest in as well as relevant selection market data
	 * @return array
	 */
	public function getSelections()
	{
		return $this->selections;
	}

	public function getBackBook()
	{
		return $this->backBook;
	}

	public function getLayBook()
	{
		return $this->layBook;
	}

	public function getWinningSelections()
	{
		if($this->winningSelections)
		{
			return $this->winningSelections;
		}

		foreach($this->selections as $key => $selection)
		{
			$this->updatedSelections[$key]['status'] = $this->selections[$key]['status'] = 'LOSER';
		}

		//randomly select for now
		if(!count($this->winningSelections))
		{
			$key = array_rand($this->selections);
			$this->updatedSelections[$key]['status'] = $this->selections[$key]['status'] = 'WINNER';

			$this->winningSelections = array($this->selections[$key]);
		}

		return $this->winningSelections;
	}

	/**
	 * Get the selections from the next snapshot of the same game. May return an empty array if the information is not available
	 * @return array
	 */
	public function getUpdatedSelections()
	{
		return $this->updatedSelections;
	}

	public function isActive()
	{
		return $this->status == 'ACTIVE' AND $this->getSelections();
	}

	/**
	 * Whether the market has a single winner, or if allows ties, if the returns are split.
	 * Or to put it another, the mathematical book is 1
	 * @return bool
	 */
	public function hasSingleWinner()
	{
		return $this->type == 'SINGLE_WINNER_OR_TIE' OR $this->type == 'WIN_ONLY';
	}

	public function getId()
	{
		return $this->id;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function getGame()
	{
		return $this->game;
	}

	public function getCommissionRate()
	{
		return $this->commissionRate;
	}

	public function getType()
	{
		return $this->type;
	}

	public function getExchange()
	{
		return $this->snapshot->getExchange();
	}

	public function getSelectionType()
	{
		return $this->selectionType;
	}

	public function getSnapshot()
	{
		return $this->snapshot;
	}
}