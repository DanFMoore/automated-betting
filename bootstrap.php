<?php
/* 
 * Sets up resources needed for classes
 */

function __autoload($class)
{
	$path=str_replace('_', '/', $class);
	include $path.'.php';
}

error_reporting(-1);
date_default_timezone_set('Europe/London');
ini_set('max_execution_time', '0');