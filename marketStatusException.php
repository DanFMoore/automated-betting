<?php

class marketStatusException extends UnexpectedValueException
{
    protected $content;
    
    public function __construct($message, $content)
    {
        parent::__construct($message);
        $this->content = $content;
    }
    
    public function getContent()
    {
        return $this->content;
    }
}