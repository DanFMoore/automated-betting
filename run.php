<?php
ini_set("display_errors", true);


/**
 * Main run controller that executes the live arb system
 */

include_once("bootstrap.php");

$games = array(
	new Game_Baccarat(true),
	new Game_Baccarat(),
	//new Game_Roulette(true),
	//new Game_Roulette(),
	new Game_Poker(true),
	new Game_Poker(),
	new Game_Omaha(),
	new Game_Racer(true),
	new Game_Racer()
);

$system = new arbitrageSystem("exchange", $games);
$system->run();
