<?php

class restClientException extends Exception
{
    protected $xmlString;
    
    /**
     * @param Exception $xmlException 
     * @param string $xmlString
     */
    public function __construct($xmlException, $xmlString)
    {
        $this->code = $xmlException->getCode();
        $this->message = $xmlException->getMessage();
        $this->xmlString = $xmlString;
    }
    
    public function getContent()
    {
        return $this->xmlString;
    }
}