<?php

/**
 * Description of ArbModel
 *
 * @author stuart
 */
class ArbModel
{
	/**
	 *
	 * @var SnapshotModel
	 */
    public $snapshotId;
	public $marketId;
	
	/**
	 *
	 * @var BetModel[]
	 */
	public $bets;
    
    public $arbType;
	public $averageReturn = 0;
	public $totalWagered = 0;
	public $totalLiability = 0;
	
	public $betTime;
	public $game;
	public $commissionPaid;




    public function __construct()
    {
		
    }
	
	public function addBetToArb(BetModel $bet)
	{
		$this->bets[$bet->selectionId] = $bet;
		
		$this->betTime = $bet->matchedDate;
		
		if($bet->betType == "BACK")
		{
			$this->arbType = "Back";
		}
		else
		{
			$this->arbType = "Lay";
		}
	}
	
	public function calculateStatistics()
	{
		
            foreach($this->bets as $bet)
            {
				$bet->calculateStatistics();
				
                $returns[] = $bet->return;
                $liabilities[] = $bet->liability;
				$marketCommission = $bet->marketCommission;
            }
            
            for($i = 0; $i < sizeof($returns); $i++)
            {
                $tempWins = $returns;
                $tempLosses = $liabilities;
                
                if($this->arbType == "Back")
                {
                    unset($tempLosses[$i]);
                    
                    $gross[] = $tempWins[$i] - array_sum($tempLosses);
                }
                
                if($this->arbType == "Lay")
                {
                    unset($tempWins[$i]);
                    
                    $gross[] = array_sum($tempWins) - $tempLosses[$i];
                }
            }
			
            $this->averageReturn = round(array_sum($gross) / sizeof($gross), 2);
            if($this->arbType == "Back")
            {
                $this->totalLiability = array_sum($liabilities);
                $this->totalWagered = array_sum($liabilities);
            }
            
            if($this->arbType == "Lay")
            {
                $this->totalLiability = max($liabilities);
                $this->totalWagered = array_sum($liabilities);
            }
			
			$this->commissionPaid = round(($this->averageReturn * $marketCommission) / 100, 2);
	}
}