<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BetModel
 *
 * @author Stuart
 */
class BetModel
{
	public $betId;
	public $betType;
	public $placedDate;
	public $matchedDate;
	public $priceRequested;
	public $size;
	
	public $marketId;
	public $channelId;
	public $channelName;
	public $gameStartDate;
	public $selectionId;
	public $selectionName;
	
	public $priceMatched;
	public $updateStamp;
	public $roundNumber;
	
	public $return;
	public $liability;
	
	public $snapshotId;
	public $totalSize;
	
	public function calculateStatistics()
	{
		if(isset($this->totalSize)) //totalSize is the column name from the view
		{
			$this->size = $this->totalSize;
		}
		
		if($this->betType == "BACK")
		{
			$this->return = round(($this->priceMatched - 1) * $this->size ,2);
			$this->liability = round($this->size ,2);
		}
		else
		{
			$this->return = round($this->size ,2);
			$this->liability = round(($this->priceMatched - 1) * $this->size ,2);
		}
	}
}
