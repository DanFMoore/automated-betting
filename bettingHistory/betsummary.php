<?php
include("Models/ArbModel.php");

$db = new PDO('mysql:host=158.255.44.63;dbname=exchangegames;charset=utf8', 'vauxhall', '\'-Y(/TD$mM4=7"I');

$id = $_GET['snapshotId'];
$result = $db->query("SELECT * FROM snapshots WHERE id=$id")->fetch(PDO::FETCH_ASSOC);
$marketXml = $result["xml"];
$betStatusXml = $result["betStatusXml"];

$result = $db->query("SELECT * FROM betrequests WHERE snapshotId=$id")->fetch(PDO::FETCH_ASSOC);
$betRequestXml = $result["xml"];
$betResponseXml = $result["responseXml"];

$arb = new ArbModel($marketXml, $betRequestXml, $betResponseXml, $betStatusXml);

foreach($arb->_market->_selections as $selectionId => $selection)
{
    if($selection->_selectionStatus != "IN_PLAY" OR !isset($arb->_bets[$selectionId])) continue;
    $selectionNames[$selectionId] = $selection->_selectionName;
    $selectionPriceMatched[$selectionId] = $arb->_bets[$selectionId]->_betPrice;
    $selectionSize[$selectionId] = $arb->_bets[$selectionId]->_betSize;
    $selectionReturn[$selectionId] = $arb->_bets[$selectionId]->_betReturn;
    
    if($arb->_arbType == "Back")
    {
        $selectionPrice[$selectionId] = $selection->_backPrices[0]["price"];
        $selectionUnmatched[$selectionId] = $selection->_backPrices[0]["amountUnmatched"];
    }
    
    if($arb->_arbType == "Lay")
    {
        $selectionPrice[$selectionId] = $selection->_layPrices[0]["price"];
        $selectionUnmatched[$selectionId] = $selection->_layPrices[0]["amountUnmatched"];
    }
    
}

foreach($arb->_bets as $selectionId => $bet)
{
    $returns[] = $bet->_betReturn;
    $liabilities[] = $bet->_betLiability;
}

for($i = 0; $i < sizeof($returns); $i++)
{
    $tempReturns = $returns;
    $tempLiabilities = $liabilities;

    if($arb->_arbType == "Back")
    {
        unset($tempLiabilities[$i]);

        $gross[] = round($tempReturns[$i] - array_sum($tempLiabilities), 2);
    }

    if($arb->_arbType == "Lay")
    {
        unset($tempReturns[$i]);

        $gross[] = round(array_sum($tempReturns) - $tempLiabilities[$i], 2);
    }
}

/*echo "<pre>";
print_r($arb);
die();*/
?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gamesarbs.php - Bet Summary</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./DemoPage1_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./DemoPage1_files/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css"></style></head>

  <body>
      <div class="container-fluid">
      <div class="row">
        
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Bet summary...</h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                    <th></th>
                    <th><?php echo implode("</th><th>", $selectionNames); ?></th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                      <th>Price</th><td><?php echo implode("</td><td>", $selectionPrice); ?></td>
                  </tr>
                  <tr>
                      <th>Unmatched</th><td>£<?php echo implode("</td><td>£", $selectionUnmatched); ?></td>
                  </tr>
                  <tr>
                      <th>Price Matched</th><td><?php echo implode("</td><td>", $selectionPriceMatched); ?></td>
                  </tr>
                  <tr>
                      <th>Size</th><td>£<?php echo implode("</td><td>£", $selectionSize); ?></td>
                  </tr>
                  <tr>
                      <th>Return</th><td>£<?php echo implode("</td><td>£", $selectionReturn); ?></td>
                  </tr>
                  <tr>
                      <th>Gross</th><td>£<?php echo implode("</td><td>£", $gross); ?></td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./DemoPage1_files/jquery.min.js"></script>
    <script src="./DemoPage1_files/bootstrap.min.js"></script>
    <script src="./DemoPage1_files/docs.min.js"></script>
  
</body></html>