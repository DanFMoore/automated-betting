<?php
include_once("Zend/Db/Adapter/Pdo/Mysql.php");
$db = new Zend_Db_Adapter_Pdo_Mysql(array(
	'host' => 'localhost',
	'username' => 'root',
	'password' => '',
	'dbname' => 'exchangegames'
));
/*
foreach($db->query("SELECT DISTINCT market FROM oldbettinghistorydata") as $row)
{
	extract($row);
	foreach($db->query("SELECT * FROM oldbettinghistorydata WHERE market = $market") as $row)
	{
		
	}
}*/

$array = array();
date_default_timezone_set("GMT");
//Check if game ids are unique in database
$totalNet = 0;
$data1["gameId"] = null;
$data1["gameDate"] = null;

foreach($db->query("SELECT * FROM flatbettinghistorydata") as $row)
{
	extract($row);

	$pieces1 = explode(" / ", $market);
	$pieces2 = explode(" (", $pieces1[1]);
	$pieces2[1] = rtrim($pieces2[1], ")");
	$pieces3 = explode(": ", $pieces2[0]);
	$pieces3[1] = (string)$pieces3[1];
	//$pieces1[0] = game
	//pieces2[1] = date
	//pieces3[1] = game id

	$selection1 = explode(" (", $selection);
	//$selection1[0] = selection

	$stamp = strtotime($pieces2[1]);
	$pieces2[1] = date("Y-m-d H:i:s", $stamp);

	//check to see if we are starting another arb, and if so, create a net entry
	if($data1["gameId"] != $pieces3[1] OR $data1["gameDate"] != $pieces2[1])
	{
		if(!empty($data1["gameId"])) //add the net profit/loss to the database
		{
			$data2["net"] = $totalNet;

			$db->update("bettinghistoryarbs", $data2, "id = $arbId");
			$totalNet = 0;
		}

		$data1["gameId"] = $pieces3[1];
		$data1["gameDate"] = $pieces2[1];
		$data1["game"] = $pieces1[0];
		
		$db->insert("bettinghistoryarbs", $data1);
		$arbId = $db->lastInsertId();

	}

	/*
	if(in_array($pieces2[1], $array) AND array_key_exists($pieces3[1], $array))
	{
		if($array[$pieces3[1]] != $pieces2[1])
		{
			echo "BALLS";
		}
	}
	else
	{
		$array[$pieces3[1]] = $pieces2[1];

	}*/

	//bettinghistorybets
	$data["arbId"] = $arbId;
	$data["betRef"] = $ref;
	$data["selection"] = $selection1[0];
	$data["betTime"] = $time;
	$data["betType"] = $type;
	$data["priceRequested"] = $priceRequested;
	$data["stake"] = $stake;
	$data["matchedPrice"] = $matchedPrice;
	$data["net"] = $net;

	$totalNet += $net;

	$db->insert("bettinghistorybets", $data);
}

//insert the last arbs net balance
$data2["net"] = $totalNet;

$db->update("bettinghistoryarbs", $data2, "id = $arbId");
$totalNet = 0;
//print_r($array);