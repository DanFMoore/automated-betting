<?php
set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});
require_once '../Models/SnapshotModel.php';
require_once '../Models/BetModel.php';

/*
 * Extracts all arb data from the snapshots table and puts it into various other tables
 * */

//Create database connection
$db = new PDO('mysql:host=localhost;dbname=exchangegames;charset=utf8', 'root', '');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Get last snapshot extracted, and store in $lastSnapshotExtracted
$query1 = $db->query("SELECT snapshotId FROM extractedsnapshots ORDER BY snapshotId DESC LIMIT 1");
$lastSnapshotExtracted = $query1->fetch();
if(empty($lastSnapshotExtracted))
{
	$lastSnapshotExtracted = 0;
}
else
{
	$lastSnapshotExtracted = $lastSnapshotExtracted["snapshotId"];
}

//Get all snapshots after the last extracted, and start to loop through them
$query2 = $db->query("SELECT * FROM snapshots WHERE id > '$lastSnapshotExtracted' AND betstatusxml IS NOT NULL");

$badRows = 0;
while($row = $query2->fetch(PDO::FETCH_ASSOC))
{
	//Prep data for the extractedArbsTable
	echo $row["id"]."\r";
	try
	{
		$snapshotModel = new SnapshotModel($row["id"], $row["date"], $row["xml"]);
	}
	catch(Exception $e)
	{
		$badRows++;
		continue;
	}
	//print_r($snapshotModel);
	//die();
	
	//Add data
	addSnapshotToDb($snapshotModel, $db);
	addMarketToDb($snapshotModel, $db);
}

echo $badRows;

function addPricesToDb(SnapshotModel $snapshotModel, PDO $db, $marketId, $selectionId, $lastRowId)
{
	$selectionQuery = "INSERT INTO extractedPrices ("
			. "selectionRowId,"
			. "priceType,"
			. "position,"
			. "amountUnmatched,"
			. "price)"
			. "VALUES ("
			. ":selectionRowId,"
			. ":priceType,"
			. ":position,"
			. ":amountUnmatched,"
			. ":price"
			. ")";
	$sth = $db->prepare($selectionQuery);
	
	$position = 1;
	foreach($snapshotModel->markets[$marketId]->selections[$selectionId]->backPrices as $prices)
	{
		try
		{
			$sth->execute(array(
				"selectionRowId" => $lastRowId,
				"priceType" => "Back",
				"position" => $position,
				"amountUnmatched" => $prices["amountUnmatched"],
				"price" => $prices["price"]
			));
		}
		catch(Exception $e)
		{
			print_r($e);
			print_r($snapshotModel);
			die();
		}
		
		$position++;
	}
	
	$position = 1;
	foreach($snapshotModel->markets[$marketId]->selections[$selectionId]->layPrices as $prices)
	{
		try
		{
			$sth->execute(array(
				"selectionRowId" => $lastRowId,
				"priceType" => "Lay",
				"position" => $position,
				"amountUnmatched" => $prices["amountUnmatched"],
				"price" => $prices["price"]
			));
		}
		catch(Exception $e)
		{
			print_r($e);
			print_r($snapshotModel);
			die();
		}
		
		$position++;
	}
}

function addSelectionsToDb(SnapshotModel $snapshotModel, PDO $db, $marketId)
{
	$selectionQuery = "INSERT INTO extractedSelections ("
			. "marketId,"
			. "selectionId,"
			. "selectionName,"
			. "selectionStatus,"
			. "selectionAmountMatched,"
			. "selectionProfitLoss)"
			. "VALUES ("
			. ":marketId,"
			. ":selectionId,"
			. ":selectionName,"
			. ":selectionStatus,"
			. ":selectionAmountMatched,"
			. ":selectionProfitLoss"
			. ")";
	$sth = $db->prepare($selectionQuery);
	
	foreach($snapshotModel->markets[$marketId]->selections as $selection)
	{
		try
		{
			$sth->execute(array(
				"marketId" => $marketId,
				"selectionId" => $selection->selectionId,
				"selectionName" => $selection->selectionName,
				"selectionStatus" => $selection->selectionStatus,
				"selectionAmountMatched" => $selection->selectionAmountMatched,
				"selectionProfitLoss" => $selection->selectionProfitLoss
			));
		}
		catch(Exception $e)
		{
			print_r($e);
			print_r($snapshotModel);
			die();
		}
		
		addPricesToDb($snapshotModel, $db, $marketId, $selection->selectionId, $db->lastInsertId());
	}
}

function addMarketToDb(SnapshotModel $snapshotModel, PDO $db)
{
	$snapshotId = $snapshotModel->snapshotId;
	
	$marketQuery = "INSERT INTO extractedMarkets ("
			. "marketId,"
			. "snapshotId,"
			. "nextMarketId,"
			. "marketStatus,"
			. "marketCommission,"
			. "marketType,"
			. "selectionsType)"
			. "VALUES ("
			. ":marketId,"
			. ":snapshotId,"
			. ":nextMarketId,"
			. ":marketStatus,"
			. ":marketCommission,"
			. ":marketType,"
			. ":selectionsType"
			. ")";
	$sth = $db->prepare($marketQuery);
	
	foreach($snapshotModel->markets as $market)
	{
		try
		{
			$sth->execute(array(
				"marketId" => $market->marketId,
				"snapshotId" => $snapshotId,
				"nextMarketId" => $market->nextMarketId,
				"marketStatus" => $market->marketStatus,
				"marketCommission" => $market->marketCommissionRate,
				"marketType" => $market->marketType,
				"selectionsType" => $market->selectionsType
			));
		}
		catch(Exception $e)
		{
			print_r($e);
			print_r($snapshotModel);
			die();
		}
		
		addSelectionsToDb($snapshotModel, $db, $market->marketId);
	}
}

function addSnapshotToDb(SnapshotModel $snapshotModel, PDO $db)
{
	$snapshotQuery = "INSERT INTO extractedSnapshots ("
			. "snapshotId, "
			. "snapshotTime, "
			. "gameType, "
			. "channelId, "
			. "gameName, "
			. "channelStatus, "
			. "gameId, "
			. "round, "
			. "bettingWindowTime, "
			. "bettingWindowPercentageComplete, "
			. "marketCurrency) "
			. "VALUES("
			. ":snapshotId,"
			. ":snapshotTime,"
			. ":gameType,"
			. ":channelId,"
			. ":gameName,"
			. ":channelStatus,"
			. ":gameId,"
			. ":round,"
			. ":bettingWindowTime,"
			. ":bettingWindowPercentageComplete,"
			. ":marketCurrency"
			. ")";
	
	$sth = $db->prepare($snapshotQuery);
	try
	{
		$sth->execute(array(
			"snapshotId" => $snapshotModel->snapshotId, 
			"snapshotTime" => $snapshotModel->snapshotTime, 
			"gameType" => $snapshotModel->gameType, 
			"channelId" => $snapshotModel->channelId, 
			"gameName" => $snapshotModel->gameName, 
			"channelStatus" => $snapshotModel->channelStatus, 
			"gameId" => $snapshotModel->gameId, 
			"round" => $snapshotModel->round, 
			"bettingWindowTime" => $snapshotModel->bettingWindowTime, 
			"bettingWindowPercentageComplete" => $snapshotModel->bettingWindowPercentageComplete, 
			"marketCurrency" => $snapshotModel->marketCurrency));
	}
	catch(Exception $e)
	{
		print_r($e);
		print_r($snapshotModel);
		die();
	}
}