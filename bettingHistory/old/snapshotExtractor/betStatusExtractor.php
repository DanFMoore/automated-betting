<?php
set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});
require_once '../Models/BetModel.php';

//Create database connection
$db = new PDO('mysql:host=localhost;dbname=exchangegames;charset=utf8', 'root', '');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Get last snapshot extracted, and store in $lastSnapshotExtracted
$query1 = $db->query("SELECT snapshotId FROM extractedbets ORDER BY snapshotId DESC LIMIT 1");
$lastSnapshotExtracted = $query1->fetch();
if(empty($lastSnapshotExtracted))
{
	$lastSnapshotExtracted = 0;
}
else
{
	$lastSnapshotExtracted = $lastSnapshotExtracted["snapshotId"];
}
//Get all snapshots after the last extracted, and start to loop through them
$query2 = $db->query("SELECT id, betStatusXml FROM snapshots WHERE id > '$lastSnapshotExtracted' AND betstatusxml IS NOT NULL");

//Array of bad snapshotIds
$badSnapshots = array(12, 241, 245, 360, 1301, 1470, 3374, 12888, 14745, 16443);

while($row = $query2->fetch(PDO::FETCH_ASSOC))
{
	echo $row["id"]."\r";
	$betStatus = new SimpleXMLElement($row["betStatusXml"]);
	
	$betQuery = "INSERT INTO extractedBets ("
			. "betId,betType,"
			. "placedDate,"
			. "matchedDate,"
			. "priceRequested,"
			. "size,"
			. "marketId,"
			. "channelId,"
			. "channelName,"
			. "gameStartDate,"
			. "selectionId,"
			. "selectionName,"
			. "priceMatched,"
			. "updateStamp,"
			. "roundNumber,"
			. "snapshotId)"
			. "VALUES("
			. ":betId,"
			. ":betType,"
			. ":placedDate,"
			. ":matchedDate,"
			. ":priceRequested,"
			. ":size,"
			. ":marketId,"
			. ":channelId,"
			. ":channelName,"
			. ":gameStartDate,"
			. ":selectionId,"
			. ":selectionName,"
			. ":priceMatched,"
			. ":updateStamp,"
			. ":roundNumber,"
			. ":snapshotId"
			. ")";
	$sth = $db->prepare($betQuery);
	
	foreach($betStatus->betSnapshotItem as $betSnapshot)
	{
		$bet = new BetModel($betSnapshot);
		
		$bet->snapshotId = $row["id"];
		unset($bet->return, $bet->liability);
		
		try
		{
			$sth->execute((array)$bet);
		}
		catch(Exception $e)
		{
			//Duplicate bet data
			if($e->errorInfo[0] == 23000 AND $e->errorInfo[1] == 1062)
			{
				echo "Found duplicate: ".$bet->marketId;
				continue;
			}
			
			//Bad snapshots
			if($e->errorInfo[0] == "HY000" AND $e->errorInfo[1] == 1452)
			{
				if(in_array($row["id"], $badSnapshots)) continue;
			}
			
			print_r($e);
			die();
		}
		//die();
	}
}