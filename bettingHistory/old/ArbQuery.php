<?php

class ArbQuery
{
	/**
	 *
	 * @var PDO
	 */
	public $db;
	/**
	 * Keeps track of markets we have already had arbs on, to handle multiple arbs in betStatusXml
	 * @var type 
	 */
	public $markets;
	
	public $arbs;
	
	public function __construct(PDO $db = null)
	{
		if(is_null($db))
		{
			$this->db = new PDO('mysql:host=localhost;dbname=exchangegames;charset=utf8', 'root', '');
		}
		else
		{
			$this->db = $db;
		}
	}
	
	/**
	 * Gets all arbs between two dates
	 * @param DateTime $start YYYY-MM-DD HH:MM:SS from
	 * @param DateTime $end YYYY-MM-DD HH:MM:SS timestamp to
	 */
	public function selectByDate($start, $end)
	{
		$result = $this->db->query("
			SELECT *, SUM(size) as totalSize 
			FROM extractedBets
			WHERE placedDate > '$start' AND placedDate < '$end' 
			GROUP BY betId, selectionName");
		
		foreach($result->fetchAll(PDO::FETCH_ASSOC) as $row)
		{
			$bet = new BetModel();
			$bet->loadFromDb($row);
			$arb->addBetToArb($bet);
			
			$arb->calculateStatistics();
			$this->arbs[$row["id"]] = $arb;
		}
	}
	
	public function selectBySnapshotId($snapshotId)
	{
		$result = $this->db->query("SELECT * FROM snapshots WHERE id=$snapshotId");
		
		foreach($result->fetchAll(PDO::FETCH_ASSOC) as $row)
		{
			$betStatus = new SimpleXMLElement($row["betStatusXml"]);
			
			$arb = new ArbModel(new SnapshotModel($row["xml"]), (string)$betStatus->betSnapshotItem[0]->selectionReference->marketReference->marketId);
			
			foreach($betStatus->betSnapshotItem as $betSnapshot)
			{
				$bet = new BetModel($betSnapshot);
				$arb->addBetToArb($bet);
			}
			
			$arb->calculateStatistics();
			$this->arbs[$row["id"]] = $arb;
		}
	}
}
