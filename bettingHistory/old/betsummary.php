<?php
date_default_timezone_set('Europe/London');

include "Models/SnapshotModel.php";
include "Models/BetModel.php";
include("Models/ArbModel.php");
include "ArbQuery.php";
include "../restClient.php";

$id = $_GET['snapshotId'];

$arbQuery = new ArbQuery();
$arbQuery->selectBySnapshotId($id);

$arb = end($arbQuery->arbs);

foreach($arb->snapshot->markets[$arb->marketId]->selections as $selectionId => $selection)
{
	//echo "<pre>";
	//print_r($selection);
	//print_r($arb->bets);
	//die();
    if($selection->selectionStatus != "IN_PLAY" OR !isset($arb->bets[$selectionId])) continue;
    $selectionNames[$selectionId] = $selection->selectionName;
    $selectionPriceMatched[$selectionId] = $arb->bets[$selectionId]->priceMatched;
    $selectionSize[$selectionId] = $arb->bets[$selectionId]->size;
    $selectionReturn[$selectionId] = $arb->bets[$selectionId]->return;
    
    if($arb->arbType == "Back")
    {
        $selectionPrice[$selectionId] = $selection->backPrices[0]["price"];
        $selectionUnmatched[$selectionId] = $selection->backPrices[0]["amountUnmatched"];
    }
    
    if($arb->arbType == "Lay")
    {
        $selectionPrice[$selectionId] = $selection->layPrices[0]["price"];
        $selectionUnmatched[$selectionId] = $selection->layPrices[0]["amountUnmatched"];
    }
    
}

foreach($arb->bets as $selectionId => $bet)
{
    $returns[] = $bet->return;
    $liabilities[] = $bet->liability;
}

for($i = 0; $i < sizeof($returns); $i++)
{
    $tempReturns = $returns;
    $tempLiabilities = $liabilities;

    if($arb->arbType == "Back")
    {
        unset($tempLiabilities[$i]);

        $gross[] = round($tempReturns[$i] - array_sum($tempLiabilities), 2);
    }

    if($arb->arbType == "Lay")
    {
        unset($tempReturns[$i]);

        $gross[] = round(array_sum($tempReturns) - $tempLiabilities[$i], 2);
    }
}

/*echo "<pre>";
print_r($arb);
die();*/
?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gamesarbs.php - Bet Summary</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./DemoPage1_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./DemoPage1_files/ie10-viewport-bug-workaround.js"></script>
	
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
	
	<script type='text/javascript'>
		$(window).load(function(){
			$('tbody').sortable();
		});
	</script>
		

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css"></style></head>

  <body>
      <div class="container-fluid">
      <div class="row">
        
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Bet summary...</h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                    <th></th>
                    <th><?php echo implode("</th><th>", $selectionNames); ?></th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                      <th>Price</th><td><?php echo implode("</td><td>", $selectionPrice); ?></td>
                  </tr>
                  <tr>
                      <th>Unmatched</th><td>£<?php echo implode("</td><td>£", $selectionUnmatched); ?></td>
                  </tr>
                  <tr>
                      <th>Price Matched</th><td><?php echo implode("</td><td>", $selectionPriceMatched); ?></td>
                  </tr>
                  <tr>
                      <th>Size</th><td>£<?php echo implode("</td><td>£", $selectionSize); ?></td>
                  </tr>
                  <tr>
                      <th>Return</th><td>£<?php echo implode("</td><td>£", $selectionReturn); ?></td>
                  </tr>
                  <tr>
                      <th>Gross</th><td>£<?php echo implode("</td><td>£", $gross); ?></td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./DemoPage1_files/jquery.min.js"></script>
    <script src="./DemoPage1_files/bootstrap.min.js"></script>
    <script src="./DemoPage1_files/docs.min.js"></script>
  
</body></html>