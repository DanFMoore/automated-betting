<?php
include_once("SVGGraph/SVGGraph.php");
include_once("Zend/Db/Adapter/Pdo/Mysql.php");
$db = new Zend_Db_Adapter_Pdo_Mysql(array(
	'host' => 'localhost',
	'username' => 'root',
	'password' => '',
	'dbname' => 'exchangegames'
));

date_default_timezone_set("GMT");

$settings = array(
	"back_colour" => "#ffffff",
	"back_stroke_width" => 0,
	//"back_round" => 10,
	//"back_stroke_colour" => "#6699FF",
	"pad_top" => 25,
	"pad_bottom" => 25,
	"pad_left" => 100,
	"pad_right" => 100,

	"axis_font" => "Verdana",
	"axis_font_size" => 12,
);

dailyProfit();
totalStaked();
averageDailyProfit();
gameProfit();
totalProfit();

function dailyProfit()
{
	global $db;

	$totalProfit["Monday"] = 0;
	$totalProfit["Tuesday"] = 0;
	$totalProfit["Wednesday"] = 0;
	$totalProfit["Thursday"] = 0;
	$totalProfit["Friday"] = 0;
	$totalProfit["Saturday"] = 0;
	$totalProfit["Sunday"] = 0;

	foreach($db->query("SELECT gameDate, net FROM bettinghistoryarbs") as $row)
	{
		extract($row);
		$stamp = strtotime($gameDate);
		$day = date("l", $stamp);

		$totalProfit[$day] += $net;
	}

	print_r($totalProfit);

	/*
	//Setup graph
	$graph = new SVGGraph(700, 250, $settings);
	$graph->values = $data;

	//Render and remove annoying crosshair thing
	$graphSvg = $graph->Fetch("LineGraph");
	$graphSvg = str_replace(' cursor="crosshair"',"",$graphSvg);
	echo $graphSvg;*/
}

function averageDailyProfit()
{
	global $db;

	$totalProfit["Monday"] = 0;
	$totalProfit["Tuesday"] = 0;
	$totalProfit["Wednesday"] = 0;
	$totalProfit["Thursday"] = 0;
	$totalProfit["Friday"] = 0;
	$totalProfit["Saturday"] = 0;
	$totalProfit["Sunday"] = 0;

	$totalArbs["Monday"] = 0;
	$totalArbs["Tuesday"] = 0;
	$totalArbs["Wednesday"] = 0;
	$totalArbs["Thursday"] = 0;
	$totalArbs["Friday"] = 0;
	$totalArbs["Saturday"] = 0;
	$totalArbs["Sunday"] = 0;

	$lastDay = "";

	foreach($db->query("SELECT gameDate, net FROM bettinghistoryarbs") as $row)
	{
		extract($row);
		
		$stamp = strtotime($gameDate);
		$day = date("l", $stamp);

		$totalProfit[$day] += $net;
		if($lastDay != $day)
		{
			$totalArbs[$day]++;
			$lastDay = $day;
		}
	}

	$totalArbs[$day]++; //so that the last day is counted
	
	foreach($totalProfit as $day => $total)
	{
		$totalProfit[$day] = "£".number_format($total / $totalArbs["$day"], 2);
	}

	print_r($totalProfit);
}

function totalStaked()
{
	global $db;
	$total = 0;

	foreach($db->query("SELECT betType, stake, matchedPrice FROM bettinghistorybets") as $row)
	{
		extract($row);
		
		if($betType == "Back")
		{
			$total += $stake;
		}
		elseif($betType == "Lay")
		{
			$total += ($matchedPrice - 1) * $stake;
		}
		else
		{
			print_r($row);
			die();
		}
		
	}

	echo "£".number_format($total, 2);
}

function gameProfit()
{
	global $db;

	foreach($db->query("SELECT game, net FROM bettinghistoryarbs") as $row)
	{
		extract($row);
		$data[$game][] = $net;
	}
	foreach($data as $game => $nets)
	{
		$data1[$game] = "£".number_format(array_sum($nets), 2);
	}

	print_r($data1);
}

function totalProfit()
{
	global $db;

	$total = $db->fetchOne("SELECT sum(net) FROM bettinghistoryarbs");

	echo "£".number_format($total, 2);
}