<?php

/**
 * Takes time in a format that can be used by strtotime
 * @param string $startDate
 * @param string $endDate
 * @param string $timezone Optional
 */
function snapshotQuery($startDate, $endDate, $timezone = "Europe/London")
{
	date_default_timezone_set($timezone);
	$startDateUnix = strtotime($startDate);
	$endDateUnix = strtotime($endDate);
	
	if(!$startDateUnix OR !$endDateUnix) die("Invalid time format: $startDate/$endDate");
	
	
}