<?php
require_once '../Models/SnapshotModel.php';

/*
 * Extracts all arb data from the snapshots table and puts it into various other tables
 * */

//Create database connection
$db = new PDO('mysql:host=localhost;dbname=exchangegames;charset=utf8', 'root', '');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Get last snapshot extracted, and store in $lastSnapshotExtracted
$query1 = $db->query("SELECT snapshotId FROM extractedArbs ORDER BY snapshotId DESC LIMIT 1");
$lastSnapshotExtracted = $query1->fetch();
if(empty($lastSnapshotExtracted))
{
	$lastSnapshotExtracted = 0;
}
else
{
	$lastSnapshotExtracted = $lastSnapshotExtracted["snapshotId"];
}

//Get all snapshots after the last extracted, and start to loop through them
$query2 = $db->query("SELECT * FROM snapshots WHERE id > '$lastSnapshotExtracted' AND betstatusxml IS NOT NULL");

while($row = $query2->fetch(PDO::FETCH_ASSOC))
{
	//Prep data for the extractedArbsTable
	$snapshotModel = new SnapshotModel($row["id"], $row["date"], $row["xml"]);
	print_r($snapshotModel);
	
	
	die();
}

/**
 * NOTES:
 * -Wont know arb type until we have parsed betstatusxml
 * -Wont know market ID untwil we have parsed betstatusxml
 */