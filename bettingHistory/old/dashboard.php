<?php
date_default_timezone_set('Europe/London');

include "Models/SnapshotModel.php";
include "Models/BetModel.php";
include("newbettingHistory/Models/ArbModel.php");
include "ArbQuery.php";
include "../restClient.php";

$db = null;
//$db = new PDO('mysql:host=158.255.44.63;dbname=exchangegames;charset=utf8', 'vauxhall', '\'-Y(/TD$mM4=7"I');

//$start = time() - 86400;
//$end = time();

$start = "2011-09-29 01:09:49";
$end = "2011-09-30 11:05:31";
$arbQuery = new ArbQuery($db);
$arbQuery->selectByDate($start, $end);

foreach($arbQuery->arbs as $arb)
{
    $returns[] = $arb->averageReturn;
    $wagered[] = $arb->totalWagered;
}
//====================
$totalBets = sizeof($returns);
$totalReturn = array_sum($returns);
$averageWin = round($totalReturn / sizeof($returns), 2);
$totalWagered = array_sum($wagered);

$result3 = $db->query('SELECT `date` FROM heartbeat');
$lastActivity = $result3->fetchColumn();

$result4 = $db->query('SELECT MAX(`date`) FROM betrequests');
$lastBet = $result4->fetchColumn();

$client = new restClient();

$result4 = $client->getSingleRequest("account/snapshot");
$currentBalance = $result4->availableToBetBalance;

if((time() - strtotime($lastActivity) < 120))
{
    $green = true;
}
else
{
    $green = false;
}
//print_r(time());
//echo "<br />";
///print_r(strtotime($lastActivity));
//print_r((time() - strtotime($lastActivity)));
//die();

?>


<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gamesarbs.php - Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./DemoPage1_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./DemoPage1_files/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css"></style></head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand">Gamesarbs.php</span>
          <span class="navbar-brand" style="color:yellow;">Current balance: £<?php echo $currentBalance; ?></span>
                  <span class="navbar-brand" style="color: <?php if($green) { echo "green";} else { echo "red";} ?>">Last activity: <?php echo $lastActivity; ?></span>
		  <span class="navbar-brand">Last bet: <?php echo $lastBet; ?></span>
        </div>
        <!--<div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="http://getbootstrap.com/examples/dashboard/#">Dashboard</a></li>
            <li><a href="http://getbootstrap.com/examples/dashboard/#">Settings</a></li>
            <li><a href="http://getbootstrap.com/examples/dashboard/#">Profile</a></li>
            <li><a href="http://getbootstrap.com/examples/dashboard/#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>-->
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="dashboard.html">Dashboard</a></li>
            <!--<li><a href="http://getbootstrap.com/examples/dashboard/#">Reports</a></li>
            <li><a href="http://getbootstrap.com/examples/dashboard/#">Analytics</a></li>
            <li><a href="http://getbootstrap.com/examples/dashboard/#">Export</a></li>-->
          </ul>
          <!--<ul class="nav nav-sidebar">
            <li><a href="">Nav item</a></li>
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
            <li><a href="">More navigation</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
          </ul>-->
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">The last 24 hours...</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <!--<img data-src="holder.js/14/auto/sky" class="img-responsive" alt="14" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzBEOEZEQiIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjEwMCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojZmZmO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjQwcHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTQ8L3RleHQ+PC9zdmc+">-->
              <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200"><circle cx="100" cy="100" r="100" fill="#0D8FDB"/><text text-anchor="middle" x="100" y="100" style="fill:#fff;font-weight:bold;font-size:40px;font-family:Arial,Helvetica,sans-serif;dominant-baseline:central"><?php echo $totalBets; ?></text></svg>
			  <h4>Total Arbs</h4>
              <span class="text-muted">Total arbs in the last 24 hours</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <!--<img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="200x200" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzM5REJBQyIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjEwMCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojMUUyOTJDO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjQwcHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+wqM0NS42NTwvdGV4dD48L3N2Zz4=">-->
              <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200"><circle cx="100" cy="100" r="100" fill="#39DBAC"/><text text-anchor="middle" x="100" y="100" style="fill:#1E292C;font-weight:bold;font-size:40px;font-family:Arial,Helvetica,sans-serif;dominant-baseline:central">£<?php echo $totalReturn; ?></text></svg>
			  <h4>Total Win</h4>
              <span class="text-muted">Total winnings, based on the average win</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <!--<img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzBEOEZEQiIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjEwMCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojZmZmO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjQwcHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+wqM0LjY3PC90ZXh0Pjwvc3ZnPg==">-->
              <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200"><circle cx="100" cy="100" r="100" fill="#0D8FDB"/><text text-anchor="middle" x="100" y="100" style="fill:#fff;font-weight:bold;font-size:40px;font-family:Arial,Helvetica,sans-serif;dominant-baseline:central">£<?php echo $averageWin; ?></text></svg>
			  <h4>Average Win</h4>
              <span class="text-muted">Average amount won on each arb</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <!--<img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="200x200" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzM5REJBQyIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjEwMCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojMUUyOTJDO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjQwcHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+wqM0NTA1LjY1PC90ZXh0Pjwvc3ZnPg==">-->
              <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200"><circle cx="100" cy="100" r="100" fill="#39DBAC"/><text text-anchor="middle" x="100" y="100" style="fill:#1E292C;font-weight:bold;font-size:40px;font-family:Arial,Helvetica,sans-serif;dominant-baseline:central">£<?php echo $totalWagered; ?></text></svg>
			  <h4>Total Wagered</h4>
              <span class="text-muted">Total value of transactions</span>
            </div>
          </div>

          <h2 class="sub-header">Arbs</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Snapshot ID</th>
                  <th>Time</th>
                  <th>Game</th>
				  <th>Arb Type</th>
                  <th>Average Win</th>
				  <th>Total Wagered</th>
				  <th>Commission Paid</th>
                </tr>
              </thead>
              <tbody>
				  <?php
					foreach($arbQuery->arbs as $snapshotId => $arb)
					{
						echo "<tr><td><a href='' onclick=\"window.open('betsummary.php?snapshotId={$snapshotId}&marketId={$arb->marketId}','winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=600,height=450');\">{$snapshotId} - {$arb->marketId}</a></td>";
						echo "<td>".date("D H:i", strtotime($arb->betTime))."</td>";
						echo "<td>{$arb->game}</td>";
						echo "<td>{$arb->arbType}</td>";
						echo "<td>£{$arb->averageReturn}</td>";
						echo "<td>£{$arb->totalWagered}</td>";
						echo "<td>£{$arb->commissionPaid}</td></tr>";
					}
				  ?>
		
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./DemoPage1_files/jquery.min.js"></script>
    <script src="./DemoPage1_files/bootstrap.min.js"></script>
    <script src="./DemoPage1_files/docs.min.js"></script>
  
</body></html>