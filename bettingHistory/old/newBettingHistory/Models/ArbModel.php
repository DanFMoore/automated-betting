<?php

/**
 * Description of ArbModel
 *
 * @author stuart
 */
class ArbModel
{
	/**
	 *
	 * @var SnapshotModel
	 */
    public $snapshot;
	public $marketId;
	
	public $bets;
    
    public $arbType;
	public $averageReturn = 0;
	public $totalWagered = 0;
	public $totalLiability = 0;
	
	public $betTime;
	public $game;
	public $commissionPaid;




    public function __construct()
    {
		
    }
	
	public function addBetToArb(BetModel $bet)
	{
		$this->bets[$bet->selectionId] = $bet;
		
		$this->betTime = $bet->matchedDate;
		
		if($bet->betType == "BACK")
		{
			$this->arbType = "Back";
		}
		else
		{
			$this->arbType = "Lay";
		}
	}
	
	public function calculateStatistics()
	{
            foreach($this->bets as $selectionId => $bet)
            {
                $returns[] = $bet->return;
                $liabilities[] = $bet->liability;
				$marketId = $bet->marketId;
            }
            
            for($i = 0; $i < sizeof($returns); $i++)
            {
                $tempReturns = $returns;
                $tempLiabilities = $liabilities;
                
                if($this->arbType == "Back")
                {
                    unset($tempLiabilities[$i]);
                    
                    $gross[] = $tempReturns[$i] - array_sum($tempLiabilities);
                }
                
                if($this->arbType == "Lay")
                {
                    unset($tempReturns[$i]);
                    
                    $gross[] = array_sum($tempReturns) - $tempLiabilities[$i];
                }
            }
			
            $this->averageReturn = round(array_sum($gross) / sizeof($gross), 2);
            if($this->arbType == "Back")
            {
                $this->totalLiability = array_sum($liabilities);
                $this->totalWagered = array_sum($liabilities);
            }
            
            if($this->arbType == "Lay")
            {
                $this->totalLiability = max($liabilities);
                $this->totalWagered = array_sum($liabilities);
            }
			
			$this->commissionPaid = round(($this->averageReturn * $this->snapshot->markets[$marketId]->marketCommissionRate) / 100, 2);
	}
}