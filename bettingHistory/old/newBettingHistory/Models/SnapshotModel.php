<?php
include("Markets/AbstractMarket.php");
include("AbstractSelection.php");
/**
 * Description of SnapshotModel
 *
 * @author stuart
 */
class SnapshotModel
{
	public $snapshotId;
	public $snapshotTime;
	
    public $gameType;
	public $channelId;
	public $gameName;
	public $channelStatus;
	
	public $gameId;
	public $round;
	public $bettingWindowTime;
	public $bettingWindowPercentageComplete;
	
	public $marketCurrency;
	/**
	 *
	 * @var AbstractMarket[]
	 */
	public $markets = array();
	
	public function __construct($snapshotId, $snapshotTime, $snapshotXml)
	{
		$data = new SimpleXMLElement($snapshotXml);
		
		$this->snapshotId = (int)$snapshotId;
		$this->snapshotTime = (string)$snapshotTime;
		
		$this->gameType = (string)$data->channel->attributes()->gameType;
		$this->channelId = (string)$data->channel->attributes()->id;
		$this->gameName = (string)$data->channel->attributes()->name;
		$this->channelStatus = (string)$data->channel->status;
		
		$this->gameId = (string)$data->channel->game->attributes()->id;
		$this->round = (string)$data->channel->game->round;
		$this->bettingWindowTime = (string)$data->channel->game->bettingWindowTime;
		$this->bettingWindowPercentageComplete = (string)$data->channel->game->bettingWindowPercentageComplete;
		
		$this->marketCurrency = (string)$data->channel->game->markets->attributes()->currency;

		foreach($data->channel->game->markets->market as $market)
		{
			$id = (string)$market->attributes()->id;
			
			if($market->marketType == "WIN_ONLY") $this->markets[$id] = new WinOnlyMarket($market);
			if($market->marketType == "ANY_NUMBER_OF_WINNERS") $this->markets[$id] = new AnyNumberOfWinnersMarket($market);
			if($market->marketType == "SINGLE_WINNER_OR_TIE") $this->markets[$id] = new SingleWinnerOrTieMarket($market);
			if($market->marketType == "VARIABLE_HANDICAP") $this->markets[$id] = new VariableHandicapMarket($market);
		}
	}
}