<?php
/**
 * Description of BetModel
 *
 * @author stuart
 */
class BetModel
{
    public $betId;
	public $betType;
	public $placedDate;
	public $matchedDate;
	public $priceRequested;
	public $size;
	
	public $marketId;
	public $channelId;
	public $channelName;
	public $gameStartDate;
	public $selectionId;
	public $selectionName;
	
	public $priceMatched;
	public $updateStamp;
	public $roundNumber;
	
	public $return;
	public $liability;
	
	public $snapshotId;
	
	public function __construct($betXml)
	{
		if(!$betXml instanceof SimpleXMLElement)
		{
			$data = new SimpleXMLElement($betXml);
		}
		else
		{
			$data = $betXml;
		}
		
		$this->betId = (string)$data->betId;
		$this->betType = (string)$data->bidType;
		$this->placedDate = (string)$data->placedDate;
		$this->matchedDate = (string)$data->matchedDate;
		$this->priceRequested = (string)$data->price;
		$this->size = (string)$data->size;
		
		$this->marketId = (string)$data->selectionReference->marketReference->marketId;
		$this->channelId = (string)$data->selectionReference->marketReference->channelId;
		$this->channelName = (string)$data->selectionReference->marketReference->channelName;
		$this->gameStartDate = (string)$data->selectionReference->marketReference->gameStartDate;
		$this->selectionId = (string)$data->selectionReference->selectionId;
		$this->selectionName = (string)$data->selectionReference->selectionName;

		$this->priceMatched = (string)$data->priceMatched;
		$this->updateStamp = (string)$data->updateStamp;
		$this->roundNumber = (string)$data->roundNumber;
		
		if($this->betType == "BACK")
		{
			$this->return = round(($this->priceMatched - 1 ) * $this->size, 2);
			$this->liability = round($this->size, 2);
		}
		elseif($this->betType == "LAY")
		{
			$this->return = round($this->size, 2);
			$this->liability = round(($this->priceMatched - 1 ) * $this->size, 2);
		}
		else
		{
			die("Unknown bet type: $this->betType");
		}
	}
	
	public function recalculateValues()
	{
		if($this->betType == "BACK")
		{
			$this->return = round(($this->priceMatched - 1 ) * $this->size, 2);
			$this->liability = round($this->size, 2);
		}
		elseif($this->betType == "LAY")
		{
			$this->return = round($this->size, 2);
			$this->liability = round(($this->priceMatched - 1 ) * $this->size, 2);
		}
		else
		{
			die("Unknown bet type: $this->betType");
		}
	}
}