<?php
include("WinOnlyMarket.php");
include("AnyNumberOfWinnersMarket.php");
include("SingleWinnerOrTieMarket.php");
include("VariableHandicapMarket.php");
/**
 * Description of AbstractMarket
 *
 * @author stuart
 */
abstract class AbstractMarket
{
    public $marketId;
    public $nextMarketId;
    public $marketStatus;
    public $marketCommissionRate;
	public $marketType = "WIN_ONLY";
	
	public $selectionsType;
	/**
	 *
	 * @var AbstractSelection[]
	 */
	public $selections = array();
    
    public function __construct($marketXml)
    {
		if(!$marketXml instanceof SimpleXMLElement)
		{
			$data = new SimpleXMLElement($marketXml);
		}
		else
		{
			$data = $marketXml;
		}
		
		$this->marketId = (string)$data->attributes()->id;
		$this->nextMarketId = (string)$data->attributes()->nextId;
		$this->marketStatus = (string)$data->status;
		$this->marketCommissionRate = (string)$data->commissionRate;
		
		$this->selectionsType = (string)$data->selections->attributes()->type;
		
		foreach($data->selections->selection as $selection)
		{
			$id = (string)$selection->attributes()->id;
			$this->selections[$id] = new AbstractSelection($selection);
		}
    }
}
