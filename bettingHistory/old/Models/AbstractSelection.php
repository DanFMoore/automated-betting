<?php
/**
 * Description of AbstractSelections
 *
 * @author stuart
 */
class AbstractSelection
{
    public $selectionId;
    public $selectionName;
    public $selectionStatus;
    public $selectionAmountMatched;
	public $selectionProfitLoss;
    public $backPrices = array();
    public $layPrices = array();


    public function __construct($selectionXml)
    {
		if(!$selectionXml instanceof SimpleXMLElement)
		{
			$data = new SimpleXMLElement($selectionXml);
		}
		else
		{
			$data = $selectionXml;
		}
		
		$this->selectionId = (string)$data->attributes()->id;
		$this->selectionName = (string)$data->name;
		$this->selectionStatus = (string)$data->status;
		$this->selectionAmountMatched = (string)$data->amountMatched;
		$this->selectionProfitLoss = (string)$data->profitLoss;
		
		foreach($data->bestAvailableToBackPrices->price as $price)
		{
			$this->backPrices[] = array("amountUnmatched" => (string)$price->attributes()->amountUnmatched, "price" => (string)$price);
		}
		
		foreach($data->bestAvailableToLayPrices->price as $price)
		{
			$this->layPrices[] = array("amountUnmatched" => (string)$price->attributes()->amountUnmatched, "price" => (string)$price);
		}
    }
}