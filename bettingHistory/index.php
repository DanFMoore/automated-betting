<?php
echo "<html>\n<head><link href='style.css' rel='stylesheet' type='text/css' />\n";
echo "<meta http-equiv='Content-Type' content='text/html;charset=utf-8' />\n</head>\n<body>\n";
include("Models/ArbModel.php");
echo "<pre>";

$db = new PDO('mysql:host=localhost;dbname=exchangegames', 'root', '');

if(isset($_GET['snapshotId']))
{
    $id = $_GET['snapshotId'];
    $result = $db->query("SELECT * FROM snapshots WHERE id=$id")->fetch(PDO::FETCH_ASSOC);
    $marketXml = $result["xml"];
    $betStatusXml = $result["betStatusXml"];
    
    $result = $db->query("SELECT * FROM betrequests WHERE snapshotId=$id")->fetch(PDO::FETCH_ASSOC);
    $betRequestXml = $result["xml"];
    $betResponseXml = $result["responseXml"];
    
    $arb = new ArbModel($marketXml, $betRequestXml, $betResponseXml, $betStatusXml);
	
	print_r($arb);
	
	$array["header"] = array("<th></th>");
	$array["prices"] = array("<td>Price</td>");
	$array["bets"] = array("<td>Bet Size</td>");
	$array["priceMatched"] = array("<td>Price Matched</td>");
	$array["return"] = array("<td>Return</td>");
	$array["totalBet"] = array("<td colspan='4' align='right'>Total Staked</td>");
	$array["theoWin"] = array("<td colspan='4' align='right'>Theoretical Win</td>");
	$array["actualWin"] = array("<td colspan='4' align='right'>Actual/Gross Win</td>");
	$array["netWin"] = array("<td colspan='4' align='right'>Net Win</td>");
	
	$otherArray["totalBet"] = 0.0;
	$otherArray["theoWin"] = 0.0;
	$otherArray["actualWin"] = 0.0;
	
	//Table header
	foreach($arb->_market->_selections as $selection)
	{
		if($selection->_selectionStatus != "IN_PLAY") continue;
		
		$selectionId = $selection->_selectionId;
		
		//header
		$array["header"][] = "<th>{$selection->_selectionName}</th>";
		
		//price
		if($arb->_arbType == "BACK")
		{
			$price = $selection->_backPrices[0]["price"];
			$array["prices"][] = "<td>{$selection->_backPrices[0]["price"]}</td>";
		}
		else
		{
			$price = $selection->_layPrices[0]["price"];
			$array["prices"][] = "<td>{$selection->_layPrices[0]["price"]}</td>";
		}
		
		//bet size
		$array["bets"][] = "<td>{$arb->_bets[$selectionId]->_betSize}</td>";
		
		//price matched
		$array["priceMatched"][] = "<td>{$arb->_bets[$selectionId]->_betPrice}</td>";
		
		//return
		$return = round(($arb->_bets[$selectionId]->_betPrice * $arb->_bets[$selectionId]->_betSize), 2);
		$array["return"][] = "<td>$return</td>";
		
		//theoretical win
		$theoWin = ($price - 1) * $arb->_bets[$selectionId]->_betSize;
		if($otherArray["theoWin"] == 0) $otherArray["theoWin"] = $theoWin;
		else $otherArray["theoWin"] = ($otherArray["theoWin"] + $theoWin) / 2;
		
		//actual win
		$actualWin = ($arb->_bets[$selectionId]->_betPrice - 1) * $arb->_bets[$selectionId]->_betSize;
		if($otherArray["actualWin"] == 0) $otherArray["actualWin"] = $actualWin;
		else $otherArray["actualWin"] = ($otherArray["actualWin"] + $actualWin) / 2;
		
		//total stake
		$otherArray["totalBet"] += $arb->_bets[$selectionId]->_betSize;
	}
	
	$netWin = round(($otherArray["actualWin"] * 0.975), 2);
	
	$array["totalBet"][] = "<td>{$otherArray["totalBet"]}</td>";
	$array["theoWin"][] = "<td>{$otherArray["theoWin"]}</td>";
	$array["actualWin"][] = "<td>{$otherArray["actualWin"]}</td>";
	$array["netWin"][] = "<td>$netWin</td>";
	
	$html = "<table><tr>";
	$html .= implode("", $array["header"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["prices"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["bets"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["priceMatched"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["return"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["totalBet"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["theoWin"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["actualWin"]);
	$html .= "</tr><tr>";
	$html .= implode("", $array["netWin"]);
	$html .= "</tr></table>";
	
	echo $html;
}
