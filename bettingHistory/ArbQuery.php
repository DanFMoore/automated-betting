<?php

class ArbQuery
{
	/**
	 *
	 * @var PDO
	 */
	public $db;
	/**
	 * Keeps track of markets we have already had arbs on, to handle multiple arbs in betStatusXml
	 * @var type 
	 */
	public $markets;
	
	/**
	 *
	 * @var ArbModel[]
	 */
	public $arbs;
	
	public function __construct(PDO $db = null)
	{
		if(is_null($db))
		{
			$this->db = new PDO('mysql:host=localhost;dbname=exchangegames;charset=utf8', 'root', '');
		}
		else
		{
			$this->db = $db;
		}
	}
	
	/**
	 * Gets all arbs between two dates
	 * @param DateTime $start YYYY-MM-DD HH:MM:SS from
	 * @param DateTime $end YYYY-MM-DD HH:MM:SS timestamp to
	 */
	public function selectByDate($start, $end)
	{
		$result = $this->db->query("
			SELECT g.*, m.marketCommission
			FROM groupedbetsgrossandloss g
			JOIN extractedmarkets m
			ON m.marketId = g.marketId AND g.gameStartDate > '$start' AND g.gameStartDate < '$end'");
		
		$currentSnapshot = null;
		$arbs = array();
		foreach($result->fetchAll(PDO::FETCH_ASSOC) as $row)
		{
			if($currentSnapshot != $row["snapshotId"])
			{
				if($currentSnapshot)
				{
					$arb->calculateStatistics();
					$arbs[] = $arb;
				}
				$arb = new ArbModel();
				$currentSnapshot = $row["snapshotId"];
				
				$arb->snapshotId = $row["snapshotId"];
				$arb->marketId = $row["marketId"];
				$arb->game = $row["channelName"];
			}
			
			$bet = new BetModel();
			foreach($row as $key => $value)
			{
				$bet->$key = $value;
			}
			
			$arb->addBetToArb($bet);
		}
		
		return $arbs;
	}
	
	public function selectBySnapshotId($snapshotId)
	{
		$result = $this->db->query("
			SELECT g.*, m.marketCommission
			FROM groupedbetsgrossandloss g
			JOIN extractedmarkets m
			ON m.marketId = g.marketId AND g.snapshotId = '$snapshotId'");
		
		$currentSnapshot = null;
		$arbs = array();
		foreach($result->fetchAll(PDO::FETCH_ASSOC) as $row)
		{
			if($currentSnapshot != $row["snapshotId"])
			{
				$arb = new ArbModel();
				$currentSnapshot = $row["snapshotId"];
				
				$arb->snapshotId = $row["snapshotId"];
				$arb->marketId = $row["marketId"];
				$arb->game = $row["channelName"];
			}
			
			$bet = new BetModel();
			foreach($row as $key => $value)
			{
				$bet->$key = $value;
			}
			
			$arb->addBetToArb($bet);
		}
		
		$arb->calculateStatistics();
		
		return $arb;
	}
}
