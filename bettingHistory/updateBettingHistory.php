<?php
include_once("Zend/Db/Adapter/Pdo/Mysql.php");
$db = new Zend_Db_Adapter_Pdo_Mysql(array(
	'host' => 'localhost',
	'username' => 'root',
	'password' => '',
	'dbname' => 'exchangegames'
));
date_default_timezone_set("GMT");

//Get the time of the last arb inserted into the betting history and convert to unix timestamp
$result = $db->fetchOne("SELECT gameDate FROM bettinghistoryarbs ORDER BY gameDate DESC LIMIT 1");
$stamp = strtotime($result);

//Take 20 minutes off, as the time in the snapshots table doesnt always tally with the game time. Convert to MySQL time
$stamp -= 1200;
$time = date("Y-m-d H:i:s", $stamp);
$totalStaked = 0;
//Start going through the database
foreach($db->query("SELECT betStatusXml FROM snapshots WHERE date > '$time' AND betStatusXml IS NOT NULL") as $row)
{
	$object = simplexml_load_string($row["betStatusXml"]);
	

	//print_r($object);
	//die();

	//Loop through each bet
	foreach($object->betSnapshotItem as $key => $betDetails)
	{
		if($betDetails->bidType == "BACK")
		{
			$object->betSnapshotItem->$key->bidType = "Back";
		}
		else
		{
			$object->betSnapshotItem->$key->bidType = "Lay";
		}

		//Prep for database insert
		$arbDetails[$key]["betRef"] = $betDetails->betId;
		$arbDetails[$key]["selection"] = $betDetails->selectionReference->selectionName;
		$arbDetails[$key]["betTime"] = $betDetails->placeDate;
		$arbDetails[$key]["betType"] = $object->betSnapshotItem->$key->bidType;
		$arbDetails[$key]["priceRequested"] = $betDetails->price;
		$arbDetails[$key]["stake"] = $betDetails->size;
		$arbDetails[$key]["matchedPrice"] = $betDetails->priceMatched;
		//$arbDetails[$key]["net"] = $net;

		//print_r($betDetails);
		//die();

		if($object->betSnapshotItem->$key->bidType == "Back")
		{
			$totalStaked += $betDetails->size;
		}
		elseif($object->betSnapshotItem->$key->bidType == "Lay")
		{
			$totalStaked += ($betDetails->priceMatched - 1) * $betDetails->size;
		}
		else
		{
			echo "d";
		}
	}


	
}
echo "£".number_format($totalStaked, 2);