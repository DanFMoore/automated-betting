<?php

abstract class abstractExchange
{
	/**
	 * Currency code
	 */
	const CURRENCY = 'GBP';

	/**
	 * Minimum bet size
	 */
	const MIN_BET_SIZE = 2;
	
	/**
	 * The maximum exposure allowed on betfair
	 */
	const MAX_EXPOSURE = 5000;

	protected $game;

	/**
	 * @var float
	 */
	protected static $accountBalance;

	/**
	 * @var abstractSnapshot
	 */
	protected $lastSnapshot;
	protected $lays = array();
	protected $backs = array();
	protected static $instances = array();

	/**
	 *
	 * @var Zend_Db_Adapter_Pdo_Mysql
	 */
	protected static $db;

	/**
	 * @return Game_Abstract
	 */
	public function getGame()
	{
		return $this->game;
	}

	/**
	 * Returns the account balance
	 * @return float
	 */
	public function getAccountBalance()
	{
		return self::$accountBalance;
	}
	
	/**
	 * Get the maximum bet amount allowed. Either the balance, or the max exposure, whichever is lower
	 * @return float
	 */
	public function getMaxBetAmount()
	{
		return min($this->getAccountBalance(), self::MAX_EXPOSURE);
	}

	/**
	 * Returns the minimum bet size
	 * @return float
	 */
	public function getMinBetSize()
	{
		return self::MIN_BET_SIZE;
	}

	public function roundPrice($price)
	{
		return $this->game->roundPrice($price);
	}

	public function getChannel()
	{
		return $this->game->getChannel();
	}

	public function addLayBet($selectionId, $size, $price, $adjustPrice = true)
	{
		$original = $price;

		if($adjustPrice)
		{
			$price *= 1.1;
			$price = $this->roundPrice($price);
		}

		$this->lays[$selectionId] = array(
			'price' => $price,
			'originalPrice' => $original,
			'size' => $size,
			'selectionId' => $selectionId,
			'maxLiability' => ($size * $price) - $size,
		);

		$this->checkBets();
	}

	public function addBackBet($selectionId, $size, $price, $adjustPrice = true)
	{
		$this->backs[$selectionId] = array(
			'price' => $adjustPrice ? 1.01 : $price,
			'originalPrice' => $price,
			'size' => $size,
			'selectionId' => $selectionId,
		);

		$this->checkBets();
	}

	public function addBet($type, $selectionId, $size, $price, $adjustPrice = true)
	{
		if($type == 'back')
		{
			$this->addBackBet($selectionId, $size, $price, $adjustPrice);
		}
		elseif($type == 'lay')
		{
			$this->addLayBet($selectionId, $size, $price, $adjustPrice);
		}
	}

	protected function checkBets()
	{
		foreach($this->lays as $lay)
		{
			if($lay['maxLiability'] > self::$accountBalance)
			{
				throw new Exception('Lay liability is too large');
			}
		}

		$size = 0;

		foreach($this->backs as $back)
		{
			$size += $back['size'];
		}

		if($size > self::$accountBalance)
		{
			throw new Exception('Back bet size is too large');
		}
	}

	public function clearBets()
	{
		$this->lays = array();
		$this->backs = array();
	}

	public function waitForEndOfRound($snapshot)
	{
		
	}

	abstract public function saveSnapshot($snapshot, $conditions);

	/**
	 * Save the conditions for betting for a snapshot
	 *
	 * @param snapshot $snapshot
	 * @param array $conditions
	 */
	protected function saveConditions($snapshot, $conditions)
	{
		foreach ($conditions as $name => $condition)
		{
			self::$db->insert('betconditions', [
				'snapshotId' => $snapshot->getId(),
				'condition' => $name,
				'state' => (int) $condition['state'],
				'value' => $condition['value'],
				'threshold' => $condition['threshold']
			]);
		}
	}

	public static function setGames($games = array())
	{
		self::$instances = array();

		foreach($games as $game)
		{
			self::$instances[] = new static($game);
		}
	}
}