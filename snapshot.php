<?php

class snapshot
{
	private $id;
	private $rawXml;
	private $updatedXml;
	private $betStatusXml;

	/* Times: */
	private $requestTime;
	private $updatedRequestTime;
	private $updatedResponseTime;
	private $responseTime;
	private $processTime;
	/* */
	
	/**
	 * @var abstractExchange
	 */
	private $exchange;
	private $roundInfo;
	private $markets;
	private $updated;

	/**
	 * Betting conditions
	 * @var array
	 */
	private $conditions = [];

	/**
	 * @param SimpleXmlElement $xml
	 * @param abstractExchange $exchange
	 */
	public function __construct($xml, $exchange, $data = array())
	{
		$this->rawXml = $xml->asXml();
		$this->exchange = $exchange;

		foreach($data as $key => $value)
		{
			$this->$key = $value;
		}

		$roundInfo = array(
			'gameId' => (int) $xml->channel->game["id"],
			'round' => (int) $xml->channel->game->round,
			'bettingTime' => (int) $xml->channel->game->bettingWindowTime,
			//add 0.5 to bettingComplete as indicated percentage of 96 might actually be 96.999~, so on average it will 0.5 too optimistic
			'bettingComplete' => (int) $xml->channel->game->bettingWindowPercentageComplete + 1
		);

		//need to add the delay from requesting the snapshot until the response to the elapsed time
		//to give the worst case for the actual elapsed time
		$roundInfo['elapsedTime'] = ($roundInfo["bettingTime"] / 100) * $roundInfo["bettingComplete"];
		$roundInfo['remainingTime'] = $roundInfo["bettingTime"] - $roundInfo['elapsedTime'];

		$this->roundInfo = $roundInfo;

		//get the market elements and produce market objects for each
		foreach($xml->channel->game->markets->market as $marketData)
		{
			$market = new market($marketData, $this);
			$this->markets[$market->getId()] = $market;
		}
	}

	public function isUpdated()
	{
		return $this->updated;
	}

	public function getMarkets()
	{
		$this->getProcessTime();
		return $this->markets;
	}

	public function getProcessTime()
	{
		if(!$this->processTime)
		{
			$this->processTime = microtime(true);
		}
		
		return $this->processTime;
	}

	public function getUpdatedXml()
	{
		return $this->updatedXml;
	}

	/**
	 * Are any of the markets open?
	 * @return bool
	 */
	public function isActive()
	{
		foreach($this->getMarkets() as $market)
		{
			//have to check markets
			if($market->getStatus() == 'ACTIVE')
			{
				return true;
			}

			return false;
		}
	}

	public function update($snapshot)
	{
		if(!$snapshot->isActive())
		{
			return;
		}

		$newInfo = $snapshot->getRoundInfo();
		
		$this->updatedResponseTime = $snapshot->getResponseTime();
		$this->updatedRequestTime = $snapshot->getRequestTime();
		$this->updatedXml = $snapshot->getRawXml();
		$this->updated = true;

		foreach($snapshot->getMarkets() as $id => $market)
		{
			if($oldMarket = $this->markets[$id])
			{
				$oldMarket->update($market);
			}
		}
	}

	public function getRequestTime()
	{
		return $this->requestTime;
	}

	public function getUpdatedRequestTime()
	{
		return $this->updatedRequestTime;
	}

	public function getResponseTime()
	{
		return $this->responseTime;
	}

	public function getGame()
	{
		return $this->exchange->getGame();
	}

	public function getUpdatedResponseTime()
	{
		return $this->updatedResponseTime;
	}

	public function getUpdatedComplete()
	{
		return $this->updatedComplete;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getDelay()
	{
		return $this->getProcessTime() - $this->getRequestTime();
	}

	/**
	 * Returns betting round information
	 * @return array
	 */
	public function getRoundInfo()
	{
		//if the snapshot hasn't been "processed" yet, then assume it is being now and add the delay on to the elapsed time
		if(!$this->processTime)
		{
			$this->roundInfo['elapsedTime'] += $this->getDelay();
		}

		return $this->roundInfo;
	}

	public function getChannel()
	{
		return $this->exchange->getChannel();
	}

	public function getExchange()
	{
		return $this->exchange;
	}

	public function getRawXml()
	{
		return $this->rawXml;
	}

	public function setConditions($conditions)
	{
		$this->conditions = $conditions;
	}

	public function save()
	{
		return $this->exchange->saveSnapshot($this, $this->conditions);
	}

	public function setBetStatusXml($xml)
	{
		$this->betStatusXml = $xml;
	}

	public function getBetStatusXml()
	{
		return $this->betStatusXml;
	}
}