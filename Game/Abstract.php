<?php

class Game_Abstract
{
	protected $channel;
	private $ticks = array();

	/**
	 * Round for betfair's ridiculous price requirements for several games now, as they've change poker
	 * @param float $price
	 * @return float
	 */
	protected function getPrecision($price)
	{
		$price = round($price, 2);
		$precision = 0;

		if ($price < 2) $precision = 0.01;
		elseif ($price < 3) $precision = 0.02;
		elseif ($price < 5) $precision = 0.05;
		elseif ($price < 10) $precision = 0.1;
		elseif ($price < 20) $precision = 0.2;
		elseif ($price < 30) $precision = 0.5;
		elseif ($price < 50) $precision = 1;
		elseif ($price < 100) $precision = 2;
		elseif ($price < 500) $precision = 5;
		elseif ($price < 1000) $precision = 10;

		return $precision;
	}
	
	/**
	 * Round the price to meet with betfair's ridiculous requirements for price increments
	 * @param float $price
	 * @return float
	 */
	public function roundPrice($price)
	{
		$price = (float) $price; //If price was something other than float/int, this function could return 1000 which is bad for lays

		if($price >= 1000)
		{
			return 1000;
		}
		elseif($price < 1.01)
		{
			return 1.01; //Should never happen
		}

		$precision = $this->getPrecision($price);
		return round(($price / $precision)) * $precision;
	}

	public function getTicks()
	{
		if($this->ticks)
		{
			return $this->ticks;
		}

		$price = 1;

		while($price < 1000)
		{
			$price = round($price + $this->getPrecision($price), 2);
			$ticks[] = $price;
		}

		return $this->ticks = $ticks;
	}

	public function getTickDifference($price1, $price2)
	{
		$ticks = $this->getTicks();
		$price1 = round($price1, 2);
		$price2 = round($price2, 2);

		$key1 = array_search($price1, $ticks);
		$key2 = array_search($price2, $ticks);

		if($key1 === false OR $key2 === false)
		{
			throw new Exception('Invalid price passed: ' . "$price1, $price2 for " . get_class($this));
		}

		return abs($key1 - $key2);
	}

	public function getChannel()
	{
		return $this->channel;
	}
}