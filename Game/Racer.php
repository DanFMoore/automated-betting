<?php

class Game_Racer extends Game_Abstract
{
	public function __construct($turbo = false)
	{
		if($turbo)
		{
			$this->channel = '1444126';
		}
		else
		{
			$this->channel = '1444116';
		}
	}
}