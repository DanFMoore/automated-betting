<?php

/**
 * Class for all betfair exchange games
 */
class exchange extends abstractExchange
{	
	/**
	 * The gap between when the last heartbeat was recorded and the next one
	 */
	const HEARTBEAT_GAP = 60;
	
	/**
	 * @var float
	 */
	private $snapshotRequestTime;

	/**
	 * @var float
	 */
	private $waitTime;

	private $betsToSave = array();
	private $betRequests = array();
	
	private static $lastHeartbeat;

	/**
	 * @var restClient
	 */
	private static $client;

	public function __construct($game = null)
	{		
		if(!self::$db)
		{
			self::$db = new db();
		}

		if(!self::$client)
		{
			self::$client = new restClient();
		}

		if(!self::$accountBalance)
		{
			self::refreshAccountBalance();
		}
		
		$this->game = $game;
	}

	public function waitForEndOfRound($snapshot)
	{
		$info = $snapshot->getRoundInfo();
		usleep(($info['remainingTime'] + 1) * 1000000);
	}

	public function requestSnapshot()
	{
		if(!self::$client->hasUrl("channels/{$this->getChannel()}/snapshot?game=false"))
		{
			$channel = $this->game->getChannel();
			self::$client->addRequest("channels/{$this->getChannel()}/snapshot?game=false");
		}
	}

	public static function getSnapshots($waitTime = null)
	{
		self::heartbeat();
		
		foreach(self::$instances as $instance)
		{
			$instance->requestSnapshot();
		}

		$snapshots = array();

		foreach(self::$instances as $instance)
		{
			if($snapshot = $instance->getSnapshot($waitTime))
			{
				$snapshots[] = $snapshot;
			}
		}

		return $snapshots;
	}
	
	protected static function heartbeat()
	{
		if(!self::$lastHeartbeat OR (time() - self::$lastHeartbeat) >= self::HEARTBEAT_GAP)
		{
			self::$db->update('heartbeat', array(
				'time' => time()
			));
			
			self::$lastHeartbeat = time();
		}
	}

	/**
	 * Return the snapshot so that all queries will return data from the same snapshot until this funciton is called again.
	 * @param $waitTime - if more than 0, then it will not allow the snapshot to be called again until that time is passed
	 * @return snapshot
	 */
	public function getSnapshot($waitTime = null, $updated = false)
	{
		//if this is to get the updated snapshot (as soon as possible after the "proper" one),
		//then get the other exchanges to request it so they are all downloaded at the same time
		if($updated)
		{
			foreach(self::$instances as $instance)
			{
				$instance->requestSnapshot();
			}
		}

		$this->sleep($waitTime);
		
		if($data = self::$client->getRequest("channels/{$this->getChannel()}/snapshot?game=false"))
		{
			$snapshot = new snapshot($data, $this, array(
				'requestTime' => self::$client->getRequestTime(),
				'responseTime' => self::$client->getResponseTime(),
			));

			if(!$updated)
			{
				$this->snapshotRequestTime = self::$client->getRequestTime();
			}

			return $snapshot;
		}
	}
	
	public function updateSnapshot($oldSnapshot)
	{
		if($oldSnapshot->isUpdated())
		{
			return;
		}

		$new = $this->getSnapshot(null, true);

		$newInfo = $new->getRoundInfo();
		$roundInfo = $oldSnapshot->getRoundInfo();

		//make sure is the same round and that the new snapshot is ahead in the betting
		if($roundInfo['round'] == $newInfo['round'] AND $roundInfo['gameId'] == $newInfo['gameId']
			AND $newInfo['bettingComplete'] >= $roundInfo['bettingComplete'])
		{
			$oldSnapshot->update($new);
		}
	}

	private function sleep($waitTime)
	{
		if(isset($waitTime))
		{
			//if the previous snapshot requested a wait time, then wait until that time has elapsed since that call before proceeding
			if($this->waitTime)
			{
				$sleepTime = $this->waitTime - (microtime(true) - $this->snapshotRequestTime);

				if($sleepTime > 0)
				{
					usleep($sleepTime * 1000000);
				}
			}

			$this->waitTime = $waitTime;
		}
	}
	
	public static function refreshAccountBalance()
	{
		$result = self::$client->getSingleRequest("account/snapshot");
		self::$accountBalance = (float) $result->availableToBetBalance;
		echo "\nBalance now: " . self::$accountBalance . date("\[Y-m-d H:i:s\]") . "\n";
	}

	/**
	 * @param snapshot $snapshot
	 * @param array $conditions list of conditions for betting or not
	 */
	public function saveSnapshot($snapshot, $conditions)
	{
		if(!$snapshot->isActive())
		{
			return;
		}

		$this->updateSnapshot($snapshot);

		self::$db->insert('snapshots', array(
			'xml' => $snapshot->getRawXml(),
			'channel' => $snapshot->getChannel(),
			'updatedXml' => $snapshot->getUpdatedXml(),
			'requestTime' => $snapshot->getRequestTime(),
			'responseTime' => $snapshot->getResponseTime(),
			'processTime' => $snapshot->getProcessTime(),
			'updatedRequestTime' => $snapshot->getUpdatedRequestTime(),
			'updatedResponseTime' => $snapshot->getUpdatedResponseTime()
		));

		$snapshot->setId(self::$db->lastInsertId());
		$this->saveConditions($snapshot, $conditions);

		echo "Snapshot saved " . date("\[Y-m-d H:i:s\]") . "\n";
	}

	/**
	 * 	Save bets in database, including full xml, both sent and returned.
	 * Also check for any betfair fuck ups about invalid exposure or whatever and deal with that
	 *
	 * @param market $market
	 * @throws Exception if no bets placed
	 */
	public function sendBets($market)
	{
		if(!count($this->backs) AND !count($this->lays))
		{
			throw new Exception('No bets placed');
		}

		$attempt = 0;

		//Recursion (of sorts. If any bets failed, this block will run again. If not (as decided from checkResult()) then it will exit.
		while(count($this->backs) OR count($this->lays))
		{
			$attempt++;

			$marketId = $market->getId();
			$round = $market->getSnapshot()->getRoundInfo();
			$currency = self::CURRENCY;

			//this is a lot easier than generating xml objects
			$xml = "<?xml version='1.0' encoding='utf-8'?>
			<postBetOrder xmlns='urn:betfair:games:api:v1'
			marketId='$marketId' round='$round[round]' currency='$currency'>";

			foreach($this->backs as $back)
			{
				$xml .= $this->generateBet($back, 'BACK');
				$this->saveBet($back, 'back', $attempt);
			}

			foreach($this->lays as $lay)
			{
				$xml .= $this->generateBet($lay, 'LAY');
				$this->saveBet($lay, 'lay', $attempt);
			}

			$xml .= "</postBetOrder>";
			$xml = str_replace("'", '"', $xml);

			/********************************************************************************************************
			 ********************************************************************************************************
			 * DANGER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 ********************************************************************************************************
			 ********************************************************************************************************/
			try
			{
				$response = self::$client->post('bet/order', $xml);
			}
			catch(restClientException $e)
			{
				self::$db->logException($e);
			}
			/********************************************************************************************************
			 ********************************************************************************************************
			 * DANGER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 ********************************************************************************************************
			 ********************************************************************************************************/

			$this->checkResponse($response);

			$this->betRequests[] = array(
				'xml' => $xml,
				'responseXml' => $response->asXml(),
				'sendTime' => self::$client->getRequestTime(),
				'responseTime' => self::$client->getResponseTime(),
				'attempt' => $attempt
			);
		}

		$snapshot = $market->getSnapshot();
		$snapshot->save();

		$matchedBets = $this->getMatchedBets($snapshot);

		foreach($this->betRequests as $request)
		{
			self::$db->insert('betrequests', $request + array('snapshotId' => $snapshot->getId()));
		}

		foreach($this->betsToSave as $bet)
		{
			self::$db->insert('bets', $bet + array(
				'snapshotId' => $snapshot->getId(),
				'averagePriceMatched' => $matchedBets[$bet['selectionId']]
			));
		}

		$this->betRequests = array();
		$this->betsToSave = array();

		//no more bets on this snapshot as the balance is not well defined.
		$this->waitForEndOfRound($snapshot);
		//acccount balance could be lower at end of betting, so need to refresh it
		$this->refreshAccountBalance();
	}

	/**
	 * Gets the information for the bets that were placed
	 * @param snapshot $snapshot
	 * @return array
	 */
	private function getMatchedBets($snapshot)
	{
		$channel = $this->getChannel();
		$xml = self::$client->getSingleRequest("bet/snapshot?betStatus=MATCHED&channelId=$channel");
		$snapshot->setBetStatusXml($xml);

		self::$db->update('snapshots', array(
			'betStatusXml' => $xml->asXml()
		), 'id=' . $snapshot->getId());

		$bets = array();

		foreach($xml->betSnapshotItem as $bet)
		{
			$bets[(int) $bet->selectionReference->selectionId] = (float) $bet->priceMatched;
		}

		return $bets;
	}

	private function saveBet($bet, $type, $attempt)
	{
		$this->betsToSave[] = $bet + array(
			'attempt' => $attempt,
			'type' => $type,
			'time' => microtime(true)
		);
	}

	/**
	 * Leaves any bets in the backs and lays arrays that need to be resent
	 *
	 * @param SimpleXMLElement $response
	 */
	private function checkResponse($response)
	{
		$betPlaced = false;
		$valid = true;

		foreach($response->totalSizeRequestResult as $bet)
		{
			switch($bet->resultCode)
			{
				case 'GAME_DOES_NOT_EXIST_OR_STALE':
				case 'BET_REJECTED_EVENT_CLOSED':
				case 'BET_REJECTED_ROUND_STALE':
					return $this->clearBets();
				
				//in an unknown error, always bet again since we can't know what happened, perhaps one went through and another didn't
				case 'UNKNOWN_ERROR':
					$betPlaced = true;
					$valid = false;
					break;
					
				case 'OK':
					$betPlaced = true;
					//no break - intentional

				case 'ALREADY_AT_TARGET_SIZE':
					break;

				default: //all other status codes - assumed to be a betfair's end
					$valid = false;
			}
		}

		//only clear bets if all results were valid (i.e. no errors at betfair's end) or no bets were actually placed
		if($valid OR !$betPlaced)
		{
			$this->clearBets();
		}

		//otherwise all bets will be attempted again. Total size requests are only allowed to be placed once, so there won't be duplicates
	}

	private function generateBet($bet, $type)
	{
		return "
			<totalSizeRequest>
				<bidType>$type</bidType>
				<price>$bet[price]</price>
				<totalSize>$bet[size]</totalSize>
				<selectionId>$bet[selectionId]</selectionId>
			</totalSizeRequest>\n";
	}
}