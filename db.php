<?php

/**
 * Wrapper for zend db to automatically reconnect connection if dropped
 */
class db
{
	/**
	 * How long to sleep for if an error
	 */
	const ERROR_SLEEP = 30;

	const HOST = 'localhost';
	const USER = 'exchangeGames';
	const PASSWORD = 'victwat1';
	const DB = 'exchangeGames';

	private $adapter;

	public function __construct()
	{
		$this->connect();
		$db = $this;

		// Log exceptions into the database and wait so don't use up 
		// resources when the error might just happen again straight away.
		set_exception_handler(function($e) use($db)
		{
			$db->logException($e);
			sleep(db::ERROR_SLEEP);
		});

		// Also log fatal errors
		register_shutdown_function(function() use($db)
		{
			if ($error = error_get_last())
			{
				$db->insert('errors', array(
					'message' => $error['message'],
					'code' => $error['type'],
					'trace' => $error['file'] . ':' . $error['line']
				));

				sleep(db::ERROR_SLEEP);
			}
			
			exit; // Make sure this function isn't called twice
		});
	}

	private function connect()
	{
		$this->adapter = new Zend_Db_Adapter_Pdo_Mysql(array(
			'host' => self::HOST,
			'username' => self::USER,
			'password' => self::PASSWORD,
			'dbname' => self::DB
		));
	}

	/**
	 * Log any exception to the database for perusing over later
	 * @param Exception $e
	 */
	public function logException($e)
	{
		$this->insert('errors', array(
			'message' => $e->getMessage(),
			'code' => $e->getCode(),
			'trace' => $e->getTraceAsString(),
			'content' => method_exists($e, 'getContent') ? $e->getContent() : null
		));
	}

	public function  __call($name, $arguments)
	{
		try
		{
			return call_user_func_array(array($this->adapter, $name), $arguments);
		}
		catch(Exception $e)
		{
			if($e->getCode() == 2006)
			{
				$this->connect();
				return $this->_call($name, $arguments);
			}

			throw $e;
		}
	}
}
