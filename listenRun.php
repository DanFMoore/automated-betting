<?php

include_once("bootstrap.php");

$games = array(
	//new Game_Poker(true),
	//new Game_Poker(),
	//new Game_Omaha(),
	//new Game_Racer(true),
	//new Game_Racer(),
	new Game_Baccarat(true),
	new Game_Baccarat()
);

exchange::setGames($games);

while($snapshots = exchange::getSnapshots())
{
	foreach($snapshots as $snapshot)
	{
		//an arb situation
		if($snapshot->isArb())
		{
			$snapshot->save();
			$snapshot->getExchange()->waitForEndOfRound();
		}
	}
}