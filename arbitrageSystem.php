<?php

class arbitrageSystem
{
	/**
	 * @var abstractExchange
	 */
	private $exchange;

	/**
	 * @var string - the class name
	 */
	private $class;

	/**
	 * @var market
	 */
	private $market;

	/**
	 * Type of arb
	 * @var string
	 */
	private $type;

	/**
	 * Conditions used for betting; all have to be true for a bet to be placed
	 * @var array
	 */
	private $conditions = [];

	/**
	 * System parameters
	 * BACK_PERCENTAGE: The amount of the bank that may be exposed on back bets
	 * LAY_PERCENTAGE: The amount of the bank that may be exposed on lay bets. Note that this is
	 * before the lay markup has been calcuated
	 * SLEEP_TIME: The amount of time inbetween each request. Note that the time to fetch the channel data,
	 * do all the calculations etc is included in the sleep time
	 * ELAPSED_TIME and REMAINING_TIME - the min amount of time elapsed or left when looking for an arb
	 * MAX_[TYPE]_GAP_TOLERANCE: The maximum amount of selections that can have their best and second best prices (for lay or arb) having a tick difference of more than 1
	 * MAX_DELAY: Requesting the data and it coming back from betfair takes time. In seconds, the longest this value can be
	 */
	CONST BACK_PERCENTAGE = 0.95;
	CONST LAY_PERCENTAGE = 0.86;
	CONST SLEEP_TIME = 0.2;
	CONST ELAPSED_TIME = 1;
	CONST REMAINING_TIME = 1;
	CONST MIN_BACK_ARB = 1;
	CONST MAX_BACK_ARB = 0.98;
	CONST MIN_LAY_ARB = 1;
	CONST MAX_LAY_ARB = 1.02;
	CONST MAX_BACK_GAP_TOLERANCE = 3;
	CONST MAX_LAY_GAP_TOLERANCE = 3;
	CONST MAX_DELAY = 0.5;

	public function __construct($class, $games)
	{
		echo date("\[Y-m-d H:i:s\]") . " Script started\n";
		$class::setGames($games);
		$this->class = $class;
	}

	/**
	 * The main method
	 */
	public function run()
	{
		$class = $this->class;

		while($snapshots = $class::getSnapshots(self::SLEEP_TIME))
		{
			foreach($snapshots as $snapshot)
			{
				foreach($snapshot->getMarkets() as $market)
				{
					$layBook = $market->getLayBook();
					$backBook = $market->getBackBook();

					echo date('Y-m-d H:i:s') . ': ' . $layBook . '/' . $backBook . ": " . $snapshot->getDelay() . "\n";

					//potential arb situation, not neccassarily adequate for betting on
					if($this->isArb($market))
					{
						$this->market = $market;
						$this->exchange = $market->getExchange();
						$snapshot = $market->getSnapshot();

						$bets = array();
						$round = $snapshot->getRoundInfo();

						if($round['round'] == 1 AND $layBook > 1)
						{
							continue;
						}

						// Narrow down the potential arbs based on the delay in fetching,
						// the elapsed time of the round and remaining time of round.
						// If any of those conditions are not met, still save the arb, but log why no bet.
						// Also populated form calcBackBetSizes() and calcLayBetSizes().
						$this->conditions = [
							'delay' => [
								'state' => $snapshot->getDelay() <= self::MAX_DELAY,
								'value' => $snapshot->getDelay(),
								'threshold' => self::MAX_DELAY
							],
							'elapsedTime' => [
								'state' => $round['elapsedTime'] >= self::ELAPSED_TIME,
								'value' => $round['elapsedTime'],
								'threshold' => self::ELAPSED_TIME
							],
							'remainingTime' => [
								'state' => $round['remainingTime'] >= self::REMAINING_TIME,
								'value' => $round['remainingTime'],
								'threshold' => self::REMAINING_TIME
							],
							'minBackArb' => [
								'state' => $backBook <= self::MIN_BACK_ARB,
								'value' => $backBook,
								'threshold' => self::MIN_BACK_ARB
							],
							'maxBackArb' => [
								'state' => $backBook >= self::MAX_BACK_ARB,
								'value' => $backBook,
								'threshold' => self::MAX_BACK_ARB
							],
							'minLayArb' => [
								'state' => $layBook >= self::MIN_LAY_ARB,
								'value' => $layBook,
								'threshold' => self::MIN_LAY_ARB
							],
							'maxLayArb' => [
								'state' => $layBook <= self::MAX_LAY_ARB,
								'value' => $layBook,
								'threshold' => self::MAX_LAY_ARB
							],
						];

						$conditions = $this->conditions;
						$betPlaced = false;

						if($conditions['delay']['state'] &&
							$conditions['elapsedTime']['state'] &&
							$conditions['remainingTime']['state'])
						{
							if($conditions['minBackArb']['state'] && $conditions['maxBackArb']['state'])
							{
								// Don't want to confuse the readout later with falses
								unset($this->conditions['minLayArb'], $this->conditions['maxLayArb']);

								if($bets = $this->calcBackBetSizes($market->getSelections()))
								{
									$this->placeBets($bets, 'back');
									$betPlaced = true;
								}
							}

							if($conditions['minLayArb']['state'] && $conditions['maxLayArb']['state'])
							{
								// Don't want to confuse the readout later with falses
								unset($this->conditions['minBackArb'], $this->conditions['maxBackArb']);

								if($bets = $this->calcLayBetSizes($market->getSelections()))
								{
									$this->placeBets($bets, 'lay');
									$betPlaced = true;
								}
							}
						}

						// The snapshot is automatically saved, with conditions, if a bet is placed.
						// It is useful to save it for testing since the constraints may change.
						if (!$betPlaced)
						{
							$snapshot->setConditions($this->conditions);
							$snapshot->save();
						}

						//go to next iteration of while($snapshots... saving or betting we are delaying the script.
						//Need fresh snapshots.
						continue 3; 
					}
				}
			}
		}
	}

	/**
	 * Can we bet on this one?
	 * @return bool
	 */
	private function isArb($market)
	{
		return $market->hasSingleWinner() AND $market->isActive() AND
			($market->getBackBook() < 1 OR $market->getLayBook() > 1);
	}

	/**
	 * Calculates the back bet sizes. Valid bet sizes are stored in $this->betSizes
	 * Returns the array of back bets if all bets are valid (all bets > £2), and FALSE if there are any invalid
	 * @return array
	 */
	private function calcBackBetSizes($selections)
	{
		$bets = array();

		//the amount of selections that have a gap of more than 1 in the best prices
		$gaps = 0;

		//First work out the max amount we can bet based on our account balance
		foreach($selections as $key => $selection)
		{
			if($selection['backs'][0]['tickDifference'] > 1)
			{
				$gaps++;
			}

			$value = round(($this->exchange->getMaxBetAmount() * self::BACK_PERCENTAGE) / $selection['backs'][0]['price'], 2);

			$this->conditions["minBetSize[$key]"] = [
				'state' => $value >= $this->exchange->getMinBetSize(),
				'value' => $value,
				'threshold' => $this->exchange->getMinBetSize()
			];

			$accountMaxWin[$key] = $value;
			$bets[$key]['selectionId'] = $selection['id'];
			$bets[$key]['price'] = $selection['backs'][0]['price']; //Do this now in case we bet later
		}

		// Only exit after iterating over all selections; need to save all of them before moving onto next criteria.
		// All previous conditions before minBetSize[] will be true at this point.
		// Will exit if any bet is below £2
		foreach ($this->conditions as $condition)
		{
			if (!$condition['state'])
			{
				return false;
			}
		}

		$this->conditions['maxBackGapTolerance'] = [
			'state' => $gaps <= self::MAX_BACK_GAP_TOLERANCE,
			'value' => $gaps,
			'threshold' => self::MAX_BACK_GAP_TOLERANCE
		];

		if (!$this->conditions['maxBackGapTolerance']['state'])
		{
			return false;
		}

		//Secondly, find the ratio of our max wins to unmatched money
		foreach($selections as $key => $selection)
		{
			$moneyRatios[] = $selection['backs'][0]['unmatched'] / $accountMaxWin[$key];
		}

		//Now find the lowest ratio
		$ratio = min($moneyRatios);

		//Finally, find bet sizes. If the ratio is lower than one, we need to round down our bet sizes
		foreach($accountMaxWin as $key => $maxWin)
		{
			if($ratio < 1)
			{
				$value = round(($maxWin * $ratio), 2);

				$this->conditions["minBetSize2[$key]"] = [
					'state' => $value >= $this->exchange->getMinBetSize(),
					'value' => $value,
					'threshold' => $this->exchange->getMinBetSize()
				];

				$bets[$key]['size'] = $value;
			}
			else
			{
				$bets[$key]['size'] = $maxWin;
			}
		}

		// Only exit after iterating over all selections; need to save all of them before moving onto next criteria.
		// All previous conditions before minBetSize2[] will be true at this point.
		// Will exit if any bet is below £2
		foreach ($this->conditions as $condition)
		{
			if (!$condition['state'])
			{
				return false;
			}
		}

		return $bets;
	}

	/**
	 * Calculates the lay bet sizes. Valid bet sizes are stored in $this->betSizes
	 * Returns TRUE if all bets are valid (all bets > £2), and FALSE if there are any invalid
	 * @return bool
	 */
	private function calcLayBetSizes($selections)
	{
		$bets = array();

		//First work out the max stakes based on our bank balance, and then find the lowest
		//Also find the largest stake, and then multiply the two to find the highest payout
		$largestPrice = 0;

		//amount of selections that have gaps of more than 1 in the best prices
		$gaps = 0;

		foreach($selections as $selection)
		{
			if($selection['lays'][0]['tickDifference'] > 1)
			{
				$gaps++;
			}

			$maxStakes[] = round(($this->exchange->getMaxBetAmount() * self::LAY_PERCENTAGE) / ($selection['lays'][0]['price'] - 1), 2);

			if($selection['lays'][0]['price'] > $largestPrice)
			{
				$largestPrice = $selection['lays'][0]['price'];
			}
		}

		$this->conditions['maxLayGapTolerance'] = [
			'state' => $gaps <= self::MAX_LAY_GAP_TOLERANCE,
			'value' => $gaps,
			'threshold' => self::MAX_LAY_GAP_TOLERANCE
		];

		if (!$this->conditions['maxLayGapTolerance']['state'])
		{
			return false;
		}

		$lowestStake = min($maxStakes);
		$highestPayout = $lowestStake * $largestPrice;

		//Secondly, find the ideal bet size (bets which ignore amount of money available), and calculate the ratio of this
		//bet to the amount of money available. Then find the lowest ratio
		foreach($selections as $key => $selection)
		{
			$value = round($highestPayout / $selection['lays'][0]['price'], 2);
			$ratio[] = $selection['lays'][0]['unmatched'] / $value;

			$this->conditions["minBetSize[$key]"] = [
				'state' => $value >= $this->exchange->getMinBetSize(),
				'value' => $value,
				'threshold' => $this->exchange->getMinBetSize()
			];

			$bets[$key]['selectionId'] = $selection['id'];
			$bets[$key]['price'] = $selection['lays'][0]['price'];
			$idealBetSizes[$key] = $value; //Store for later incase there is more money available then we have
		}

		// Only exit after iterating over all selections; need to save all of them before moving onto next criteria.
		// All previous conditions before minBetSize[] will be true at this point.
		// Will exit if any bet is below £2
		foreach ($this->conditions as $condition)
		{
			if (!$condition['state'])
			{
				return false;
			}
		}

		$ratio = min($ratio);

		//Finally, find bet sizes
		foreach($idealBetSizes as $key => $size)
		{
			if($ratio < 1)
			{
				$value = round($size * $ratio, 2);

				$this->conditions["minBetSize2[$key]"] = [
					'state' => $value >= $this->exchange->getMinBetSize(),
					'value' => $value,
					'threshold' => $this->exchange->getMinBetSize()
				];

				$bets[$key]['size'] = $value;
			}
			else
			{
				$bets[$key]['size'] = $size;
			}
		}

		// Only exit after iterating over all selections; need to save all of them before moving onto next criteria.
		// All previous conditions before minBetSize2[] will be true at this point.
		// Will exit if any bet is below £2
		foreach ($this->conditions as $condition)
		{
			if (!$condition['state'])
			{
				return false;
			}
		}

		return $bets;
	}

	/**
	 * The 'business' end of the class
	 */
	private function placeBets($bets, $type)
	{
		$this->market->getSnapshot()->setConditions($this->conditions);
		$this->exchange->clearBets();
		$this->type = $type;

		foreach($bets as $bet)
		{
			$this->exchange->addBet($type, $bet['selectionId'], $bet['size'], $bet['price']);
		}

		$this->exchange->sendBets($this->market);

		echo date("\[Y-m-d H:i:s\]") . '::';
		echo $this->market->getSnapshot()->getId() . '::';
		echo $this->exchange->getAccountBalance();

		echo "\n";
	}
}