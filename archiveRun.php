<?php
ini_set("display_errors", true);


/**
 * Run controller that executes the archive arb system to populate bet conditions
 */

include_once("bootstrap.php");

$games = array(
    new Game_Baccarat(true),
    new Game_Baccarat(),
    //new Game_Roulette(true),
    //new Game_Roulette(),
    new Game_Poker(true),
    new Game_Poker(),
    new Game_Omaha(),
    new Game_Racer(true),
    new Game_Racer()
);

// Run through with 3000 balance (around our normal balance) to save conditions for betting
archiveExchange::$fixedBalance = 3000;

$system = new arbitrageSystem("archiveExchange", $games);
archiveExchange::setDateRange('2015-08-01');

$system->run();
