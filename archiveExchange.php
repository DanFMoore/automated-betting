<?php

class archiveExchange extends abstractExchange
{
	private $query;
	private $start;
	private $end;

	const BALANCE = 2000;

	/**
	 * If set, then any profit calculations are ignored and the bets are run through without ever changing the balance
	 *
	 * @var int|float
	 */
	public static $fixedBalance = null;

	public function __construct($game = null)
	{
		if (self::$fixedBalance)
		{
			self::$accountBalance = self::$fixedBalance;
		}
		
		if(!self::$accountBalance)
		{
			self::$accountBalance = self::BALANCE;
		}

		if(!self::$db)
		{
			self::$db = new db();
		}
		
		$this->game = $game;
	}

	private function getQuery()
	{
		if(!$this->query)
		{
			$params = array($this->getChannel());
			$startWhere = '';
			$endWhere = '';

			if($this->start)
			{
				$startWhere = 'AND `date` >= ?';
				$params[] = $this->start;
			}
			if($this->end)
			{
				$endWhere = 'AND `date` <= ?';
				$params[] = $this->end;
			}

			$this->query = self::$db->query("SELECT
				*
				FROM snapshots WHERE "
				// Only need updated xml if calculating bet profit, which fixed balance doesn't do
				. (self::$fixedBalance ? '' : "updatedRequestTime AND") .
				" channel = ? $startWhere $endWhere ORDER BY id", $params);
		}

		return $this->query;
	}

	public static function setDateRange($start, $end = null)
	{
		foreach (self::$instances as $instance)
		{
			$instance->start = $start;
			$instance->end = $end;
		}
	}

	public static function getSnapshots()
	{
		$snapshots = array();

		foreach(self::$instances as $instance)
		{
			if($snapshot = $instance->getSnapshot())
			{
				$snapshots[] = $snapshot;
			}
		}

		return $snapshots;
	}

	public function getSnapshot()
	{
		if($data = $this->getQuery()->fetch())
		{
			$snapshot = new snapshot(new SimpleXmlElement($data['xml']), $this, array(
				'requestTime' => $data['requestTime'],
				'responseTime' => $data['responseTime'],
				'processTime' => $data['processTime'],
				'id' => $data['id']
			));

			// Only bother getting the updated snapshot if not fixed balance,
			// otherwise the betting calculations are not needed.
			if(!self::$fixedBalance && $data['updatedXml'])
			{
				$updated = new snapshot(new SimpleXmlElement($data['updatedXml']), $this, array(
					'requestTime' => $data['updatedRequestTime'],
					'responseTime' => $data['updatedResponseTime']
				));

				$snapshot->update($updated);
			}

			return $snapshot;
		}
	}

	public function sendBets($market)
	{
		if (!self::$fixedBalance)
		{
			$profit = $this->getProfit($market);
			self::$accountBalance += $profit;
		}
		
		$this->clearBets();
	}

	/**
	 * Update a snapshot with the conditions, if passed in
	 *
	 * @param snapshot $snapshot
	 * @param array $conditions
	 */
	public function saveSnapshot($snapshot, $conditions)
	{
		if ($conditions)
		{
			$this->saveConditions($snapshot, $conditions);
			echo "Snapshot saved " . $snapshot->getId() . "\n";
		}
	}

	/**
	 * Get the estimated profit for the given bets
	 */
	private function getProfit($market)
	{
		$profit = 0;
		$winners = count($market->getWinningSelections());
		
		if(!$updated = $market->getUpdatedSelections())
		{
			throw new Exception('No updated selections available');
		}

		if(!$winners)
		{
			throw new Exception('No winners set');
		}

		foreach($this->backs as $selectionId => $back)
		{
			if($updated[$selectionId]['status'] == 'LOSER')
			{
				$profit -= $back['size'];
			}
			else
			{
				$selectionProfit = $this->calcProfit($updated[$selectionId]['backs'], $back, $winners);
				$profit += $selectionProfit;
			}
		}

		foreach($this->lays as $selectionId => $lay)
		{
			if($updated[$selectionId]['status'] == 'LOSER')
			{
				$profit += $lay['size'];
			}
			else
			{
				$selectionLiability = $this->calcProfit($updated[$selectionId]['lays'], $lay, $winners);
				$profit -= $selectionLiability;
			}
		}
		
		$profit -= $profit * ($market->getCommissionRate() / 100);
		return round($profit, 2);
	}

	/**
	 * Calculates both profit for backs and liability for lays. They are the same thing essentially, just opposite. Hence the name of this function.
	 * Go through all prices in the updated selection, figuring out the profit/liability at each until all of our money is matched.
	 * @throws Exception for extreme edge cases where there is not enough money in the three best prices, as we don't save those prices.
	 * @param array $prices
	 * @param array $bet
	 * @return float The profit/liability for these prices
	 */
	private function calcProfit($prices, $bet, $winners)
	{
		$size = $bet['size'];
		$profit = 0;

		if(!$prices)
		{
			throw new Exception('Invalid prices array');
		}

		foreach($prices as $price)
		{
			//casting in case I get any float weirdness where 0.0 doesn't actually equal 0 for example.
			//if no size left then all of our money has been matched
			if(!(string) $size)
			{
				break;
			}

			//the amount matched at this price will either be the total amount bet if the unmatched amount is a larger than the size bet,
			//or all of the unmatched amount if the size bet if larger than the unmatched amount.
			//In the second case, need to figure out the profit at this price, then move on to the next best price, keeping a running total
			if($price['unmatched'] < $size)
			{
				$amountMatched = $price['unmatched'];
			}
			else
			{
				$amountMatched = $size;
			}

			$profit += $amountMatched * ($price['price'] / $winners);
			$size -= $amountMatched;
		}

		if((string) $size)
		{
			throw new Exception('Not enough unmatched money at saved prices to match bets');
		}

		//remove the original stake for profit as opposed to total return
		return $profit - $bet['size'];
	}
}
