/*
Navicat MySQL Data Transfer

Source Server         : VPS
Source Server Version : 50513
Source Host           : gamesbooksfilms.com:3306
Source Database       : exchangeGames

Target Server Type    : MYSQL
Target Server Version : 50513
File Encoding         : 65001

Date: 2011-09-28 23:59:56
*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `extracted_bets` (
  `snapshotId` INT(11) NULL DEFAULT NULL,
  `betId` BIGINT(20) NOT NULL,
  `bidType` VARCHAR(50) NULL DEFAULT NULL,
  `placedDate` DATETIME NULL DEFAULT NULL,
  `matchedDate` DATETIME NULL DEFAULT NULL,
  `price` DECIMAL(10,2) NULL DEFAULT NULL,
  `size` DECIMAL(10,2) NULL DEFAULT NULL,
  `marketId` INT(11) NULL DEFAULT NULL,
  `channelId` INT(11) NULL DEFAULT NULL,
  `channelName` VARCHAR(50) NULL DEFAULT NULL,
  `gameStartDate` DATETIME NULL DEFAULT NULL,
  `selectionId` INT(11) NULL DEFAULT NULL,
  `selectionName` VARCHAR(50) NULL DEFAULT NULL,
  `priceMatched` DECIMAL(10,2) NULL DEFAULT NULL,
  `updateStamp` BIGINT(20) NULL DEFAULT NULL,
  `roundNumber` INT(11) NULL DEFAULT NULL,
  UNIQUE INDEX `betId_size_priceMatched_updateStamp` (`betId`, `size`, `priceMatched`, `updateStamp`),
  INDEX `snapshotId` (`snapshotId`),
  INDEX `marketId` (`marketId`),
  INDEX `channelId` (`channelId`),
  INDEX `placedDate` (`placedDate`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB
;


CREATE TABLE `extracted_markets` (
  `snapshotId` INT(11) NOT NULL,
  `marketId` INT(11) NOT NULL,
  `marketNextId` INT(11) NOT NULL,
  `marketCurrency` VARCHAR(50) NOT NULL,
  `marketStatus` VARCHAR(50) NOT NULL,
  `marketCommissionRate` DECIMAL(4,2) NOT NULL,
  `marketType` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`snapshotId`, `marketId`),
  INDEX `marketId` (`marketId`),
  CONSTRAINT `snapshotIdFK` FOREIGN KEY (`snapshotId`) REFERENCES `extracted_snapshots` (`snapshotId`) ON DELETE CASCADE
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
;


CREATE TABLE `extracted_prices` (
  `snapshotId` INT(11) NOT NULL,
  `marketId` INT(11) NOT NULL,
  `selectionId` INT(11) NOT NULL,
  `pricePosition` INT(11) NOT NULL,
  `priceType` VARCHAR(50) NOT NULL DEFAULT '',
  `priceAmountUnmatched` DECIMAL(10,2) NULL DEFAULT NULL,
  `pricePrice` DECIMAL(10,2) NULL DEFAULT NULL,
  PRIMARY KEY (`snapshotId`, `marketId`, `selectionId`, `pricePosition`, `priceType`),
  INDEX `snapshotId` (`snapshotId`),
  CONSTRAINT `snapshotIdMarketIdSelectionId` FOREIGN KEY (`snapshotId`, `marketId`, `selectionId`) REFERENCES `extracted_selections` (`snapshotId`, `marketId`, `selectionId`) ON DELETE CASCADE
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
;


CREATE TABLE `extracted_selections` (
  `snapshotId` INT(11) NOT NULL,
  `marketId` INT(11) NOT NULL,
  `selectionId` INT(11) NOT NULL,
  `selectionType` VARCHAR(50) NOT NULL,
  `selectionName` VARCHAR(50) NOT NULL,
  `selectionStatus` VARCHAR(50) NOT NULL,
  `selectionAmountMatched` DECIMAL(10,2) NOT NULL,
  `selectionProfitLoss` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`snapshotId`, `marketId`, `selectionId`),
  INDEX `snapshotId` (`snapshotId`),
  INDEX `marketId` (`marketId`),
  INDEX `selectionId` (`selectionId`),
  CONSTRAINT `snapshotIdMarketIdFK` FOREIGN KEY (`snapshotId`, `marketId`) REFERENCES `extracted_markets` (`snapshotId`, `marketId`) ON DELETE CASCADE
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
  ROW_FORMAT=COMPRESSED
;


CREATE TABLE `extracted_snapshots` (
  `snapshotId` INT(11) NOT NULL,
  `channelId` INT(11) UNSIGNED NOT NULL,
  `channelGameType` VARCHAR(50) NOT NULL,
  `channelName` VARCHAR(50) NOT NULL,
  `channelStatus` VARCHAR(50) NOT NULL,
  `gameId` INT(11) NOT NULL,
  `gameRound` INT(11) NOT NULL,
  `gameBettingWindowTime` INT(11) NOT NULL,
  `gameBettingWindowPercentageComplete` INT(11) NOT NULL,
  `snapshotRequestTime` DECIMAL(20,10) NULL DEFAULT NULL,
  `snapshotResponseTime` DECIMAL(20,10) NULL DEFAULT NULL,
  `snapshotProcessTime` DECIMAL(20,10) NULL DEFAULT NULL,
  `snapshotUpdateRequestTime` DECIMAL(20,10) NULL DEFAULT NULL,
  `snapshotUpdateResponseTime` DECIMAL(20,10) NULL DEFAULT NULL,
  `snapshotDate` TIMESTAMP NULL DEFAULT NULL,
  `xmlTruncated` TINYINT(4) NULL DEFAULT '0',
  PRIMARY KEY (`snapshotId`)
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
  ROW_FORMAT=COMPRESSED
;


-- ----------------------------
-- Table structure for `betrequests`
-- ----------------------------
DROP TABLE IF EXISTS `betrequests`;
CREATE TABLE `betrequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `snapshotId` int(11) DEFAULT NULL,
  `xml` text,
  `responseXml` text,
  `sendTime` decimal(20,10) DEFAULT NULL,
  `responseTime` decimal(20,10) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of betrequests
-- ----------------------------

-- ----------------------------
-- Table structure for `bets`
-- ----------------------------
DROP TABLE IF EXISTS `bets`;
CREATE TABLE `bets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `snapshotId` int(11) DEFAULT NULL,
  `averagePriceMatched` float DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `time` decimal(20,10) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `originalPrice` float DEFAULT NULL,
  `size` float DEFAULT NULL,
  `selectionId` int(11) DEFAULT NULL,
  `maxLiability` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bets
-- ----------------------------

-- ----------------------------
-- Table structure for `prices`
-- ----------------------------
DROP TABLE IF EXISTS `prices`;
CREATE TABLE `prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float DEFAULT NULL,
  `unmatched` float DEFAULT NULL,
  `selectionId` int(11) DEFAULT NULL,
  `snapshotId` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `updated` tinyint(4) DEFAULT NULL,
  `type` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prices
-- ----------------------------

-- ----------------------------
-- Table structure for `selections`
-- ----------------------------
DROP TABLE IF EXISTS `selections`;
CREATE TABLE `selections` (
  `id` int(11) NOT NULL DEFAULT '0',
  `snapshotId` int(11) NOT NULL DEFAULT '0',
  `amountMatched` float DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `updatedAmountMatched` float DEFAULT NULL,
  PRIMARY KEY (`id`,`snapshotId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of selections
-- ----------------------------

-- ----------------------------
-- Table structure for `snapshots`
-- ----------------------------
DROP TABLE IF EXISTS `snapshots`;
# noinspection SqlNoDataSourceInspection
CREATE TABLE `snapshots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` int(11) DEFAULT NULL,
  `xml` text,
  `updatedXml` text,
  `requestTime` decimal(20,10) DEFAULT NULL,
  `responseTime` decimal(20,10) DEFAULT NULL,
  `processTime` decimal(20,10) DEFAULT NULL,
  `updatedRequestTime` decimal(20,10) DEFAULT NULL,
  `updatedResponseTime` decimal(20,10) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `betStatusXml` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of snapshots
-- ----------------------------

-- ----------------------------
-- Table structure for `betconditions`
-- ----------------------------
CREATE TABLE `betconditions` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `snapshotId` INT(11) NOT NULL DEFAULT '0',
  `condition` VARCHAR(50) NOT NULL DEFAULT '0',
  `state` TINYINT(4) NOT NULL DEFAULT '0',
  `threshold` DOUBLE NOT NULL DEFAULT '0',
  `value` DOUBLE NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `FK_betconditions_extracted_snapshots` (`snapshotId`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB
  AUTO_INCREMENT=7
;